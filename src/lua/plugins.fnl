(require "lazy-bootstrap")
(local lazy (require "lazy"))

(lazy.setup
  [
   ; ==> TOSORT

   ; {:url "https://github.com/chrisgrieser/nvim-early-retirement" :config true :event :VeryLazy}

   {:url "https://github.com/anuvyklack/pretty-fold.nvim" :config {:fill_char " "}}
   ; { :url "https://github.com/zhou13/vim-easyescape" }

   {:url "https://github.com/skywind3000/asyncrun.vim"}

   ; ==> Navigating

   { :url "https://github.com/akinsho/toggleterm.nvim"}

   { :url "https://github.com/rafaqz/ranger.vim"}

   ; ==> UI

   { :url "https://github.com/folke/twilight.nvim" :name :twilight :config true}
   { :url "https://github.com/folke/zen-mode.nvim" :name :zen-mode :config true}

   ; { :url "https://github.com/JellyApple102/easyread.nvim" :name "easyread" :config true }

   {:url "https://github.com/folke/which-key.nvim"
    :dependencies
    [{:url "https://github.com/echasnovski/mini.icons"}
     {:url "https://github.com/nvim-tree/nvim-web-devicons"}]}

   { :url "https://github.com/luukvbaal/statuscol.nvim"}

   ; { :url "https://github.com/norcalli/nvim-colorizer.lua"}
   {:url "https://github.com/chrisbra/Colorizer"}

   ; { :url "https://github.com/lukas-reineke/indent-blankline.nvim"} 

   { :url "https://github.com/tpope/vim-fugitive"}
   { :url "https://github.com/rbong/vim-flog"}
   { :url "https://github.com/lewis6991/gitsigns.nvim" :name :gitsigns :config true}

   { :url "https://github.com/hrsh7th/nvim-cmp"
    :dependencies
    [{ :url "https://github.com/hrsh7th/cmp-nvim-lsp"}
     { :url "https://github.com/hrsh7th/cmp-nvim-lsp-document-symbol"}
     { :url "https://github.com/hrsh7th/cmp-path"}
     { :url "https://github.com/uga-rosa/cmp-dictionary"}
     { :url "https://github.com/hrsh7th/cmp-path"}
     { :url "https://github.com/saadparwaiz1/cmp_luasnip"}
     { :url "https://github.com/ray-x/cmp-treesitter"}
     { :url "https://github.com/chrisgrieser/cmp-nerdfont"}
     { :url "https://github.com/amarakon/nvim-cmp-lua-latex-symbols"}
     { :url "https://github.com/lukas-reineke/cmp-rg"}
     { :url "https://github.com/hrsh7th/cmp-path"}
     { :url "https://github.com/rcarriga/cmp-dap"}
     { :url "https://github.com/quangnguyen30192/cmp-nvim-tags"}
     { :url "https://github.com/aspeddro/cmp-pandoc.nvim" :name "cmp_pandoc" :config true}
     { :url "https://github.com/PaterJason/cmp-conjure"}]}

   { :url "https://github.com/nvim-lualine/lualine.nvim"
    :dependencies [ { :url "https://github.com/nvim-tree/nvim-web-devicons"}]}

   { :url "https://github.com/akinsho/bufferline.nvim"
    :dependencies [ { :url "https://github.com/nvim-tree/nvim-web-devicons"}]}

   { :url "https://github.com/hiphish/rainbow-delimiters.nvim"
    :dependencies [{:url "https://github.com/nvim-treesitter/nvim-treesitter"}]}

   ; ==> Edit
   { :url "https://github.com/tpope/vim-abolish"}
   ; { :url "https://github.com/hrsh7th/nvim-pasta" }

   {:url "https://github.com/m4xshen/autoclose.nvim"}

   { :url "https://github.com/kylechui/nvim-surround" :name "nvim-surround" :config true}

   { :url "https://github.com/L3MON4D3/LuaSnip" :name "luasnip"
    :dependencies [ { :url "https://github.com/rafamadriz/friendly-snippets"}]}

   { :url "https://github.com/windwp/nvim-ts-autotag" :name "nvim-ts-autotag" :config true
    :dependencies [ {:url "https://github.com/nvim-treesitter/nvim-treesitter"}]}

   { :url "https://github.com/chrisbra/unicode.vim"}
   { :url "https://github.com/gyim/vim-boxdraw"}

   { :url "https://github.com/godlygeek/tabular"}

   { :url "https://github.com/sanfusu/neovim-undotree"}

   { :url "https://github.com/terrortylor/nvim-comment"
    :name "nvim_comment" :opts { :comment_empty false}}
   ; { :url  "https://github.com/tpope/vim-commentary"}

   ; { :url "https://github.com/eraserhd/parinfer-rust" :build "cargo build --release" }
   ; { :url "https://github.com/harrygallagher4/nvim-parinfer-rust" }

   { :url "https://github.com/gpanders/nvim-parinfer"}

   {:url "https://github.com/Grazfather/sexp.nvim"
    :dependencies [{:url "https://github.com/tpope/vim-repeat"}]}

   ; {:url "https://github.com/julienvincent/nvim-paredit"
   ;  :config (lambda [] ((. (require :nvim-paredit) :setup)))} 

   ; {:url "https://github.com/julienvincent/nvim-paredit-fennel"
   ;  :config (lambda [] ((. (require :nvim-paredit-fennel) :setup)))
   ;  :ft :fennel :dependencies [{:url "https://github.com/julienvincent/nvim-paredit"}]}

   ; {:url "https://git.elenq.tech/nvim-paredit-scheme/"
   ;  :config (lambda [] ((. (require :nvim-paredit-scheme) :setup) (require :nvim-paredit)))
   ;  :ft :fennel :dependencies [{:url "https://github.com/julienvincent/nvim-paredit"}]}


   { :url "https://github.com/nicwest/vim-camelsnek"}

   ; ==> Sys

   { :url "https://github.com/nvim-lua/plenary.nvim"}

   ; ==> Finding


   ; (if (vim.fn.executable "fzy") { :url "https://github.com/mfussenegger/nvim-fzy"}
   ; (vim.fn.executable "fzf") { :url "https://github.com/vijaymarupudi/nvim-fzf"})
   { :url "https://github.com/ibhagwan/fzf-lua"
    :dependencies [ { :url "https://github.com/nvim-tree/nvim-web-devicons"}]}

   {:url "https://github.com/ThePrimeagen/harpoon"
    :branch :harpoon2
    :dependencies [{:url "https://github.com/nvim-lua/plenary.nvim"}]}

   ; ==> Code server

   ; { :url "https://github.com/monkoose/nvlime"
   ; :dependencies [ { :url "https://github.com/monkoose/parsley"}]}

   ; Dap
   {:url "https://github.com/mfussenegger/nvim-dap"}

   {:url "https://github.com/rcarriga/nvim-dap-ui"
    :dependencies
    [{:url "https://github.com/mfussenegger/nvim-dap"}
     {:url "https://github.com/nvim-neotest/nvim-nio"}
     {:url "https://github.com/folke/neodev.nvim" :name "neodev"
      :config {:library {:plugins ["nvim-dap-ui"] :types true}}}]}

   { :url "https://github.com/theHamsta/nvim-dap-virtual-text" :name "nvim-dap-virtual-text" :config true}

   ; Lsp
   { :url "https://github.com/VonHeikemen/lsp-zero.nvim"
    :branch "v1.x"
    :dependencies
    [ { :url "https://github.com/neovim/nvim-lspconfig"}
      { :url "https://github.com/williamboman/mason.nvim"}
      { :url "https://github.com/williamboman/mason-lspconfig.nvim"}]}

   {:url "https://github.com/antosha417/nvim-lsp-file-operations"
    :config
    {:operations
      {:willRenameFiles true
       :didRenameFiles true
       :willCreateFiles true
       :didCreateFiles true
       :willDeleteFiles true
       :didDeleteFiles true}}
    :dependencies
    [{:url "nvim-lua/plenary.nvim"}]}


   { :url "https://github.com/williamboman/mason.nvim" :name "mason" :config true
    :dependencies
    [{ :url "https://github.com/neovim/nvim-lspconfig"}
     {:url "https://github.com/williamboman/mason-lspconfig.nvim"}]}

   ; Treesitter
   { :url "https://github.com/nvim-treesitter/nvim-treesitter" :build ":TSUpdate"}

   ; Other
   { :url "https://github.com/clojure-vim/vim-jack-in"
     :dependencies [{:url "https://github.com/radenling/vim-dispatch-neovim"}
                    {:url "https://github.com/tpope/vim-dispatch"}]}

   { :url "https://github.com/Olical/aniseed"}
   { :url "https://github.com/Olical/conjure"}
  
   { :url "https://git.sr.ht/~efraim/guix.vim"}

   ; Util
   { :url "https://github.com/mfussenegger/nvim-lint"}
   { :url "https://github.com/mhartington/formatter.nvim"}

   ; ==> Utils

   ; { :url "https://github.com/airblade/vim-rooter"}

   ; ==> Preview

   { :url "https://github.com/jbyuki/nabla.nvim"}
   {:url "https://github.com/iamcco/markdown-preview.nvim"}
   ; {:url "https://github.com/OXY2DEV/markview.nvim"
   ;  :dependencies
   ;  [{:url "https://github.com/nvim-treesitter/nvim-treesitter"}
   ;   {:url "https://github.com/nvim-tree/nvim-web-devicons"}]}
      
     

   ; ==> Spellcheck

      ; ==> Spellcheck


   ; ==> Syntax

   { :url "https://github.com/folke/todo-comments.nvim"
    :name "todo-comments" :config true
    :dependencies [ { :url "https://github.com/nvim-lua/plenary.nvim"}]}

   ; Conf
   { :url "https://github.com/mrk21/yaml-vim"}
   { :url "https://github.com/cuducos/yaml.nvim"
    :ft [ "yaml"]
    :dependencies
    [{ :url "https://github.com/nvim-treesitter/nvim-treesitter"}]}
   { :url "https://github.com/pedrohdz/vim-yaml-folds"}

   { :url "https://github.com/chrisbra/csv.vim"}
   { :url "https://github.com/jwadamson/vim-jsonl"}
   { :url "https://github.com/chr4/nginx.vim"}
   { :url "https://github.com/digitaltoad/vim-pug"}
   { :url "https://github.com/cespare/vim-toml"}
   { :url "https://github.com/LnL7/vim-nix"}
   { :url "https://github.com/hashivim/vim-terraform"}

   ; Prog
   { :url "https://github.com/clojure-vim/clojure.vim"}
   { :url "https://github.com/typedclojure/vim-typedclojure"}
   { :url "https://github.com/bakpakin/fennel.vim"}
   { :url "https://github.com/leafgarland/typescript-vim"}
   { :url "https://github.com/pangloss/vim-javascript"}
   { :url "https://github.com/Olical/vim-scheme"}
   { :url "https://github.com/dart-lang/dart-vim-plugin"}
   { :url "https://github.com/tpope/vim-cucumber"}
   { :url "https://github.com/HiPhish/guile.vim"}

   ; Framework
   { :url "https://github.com/posva/vim-vue"}

   ; Other

   ; { :url "https://github.com/jladan/nvim-latex" :config (lambda [plug] ((. (require plug.name) :setup_document)))}
   ; Markdown

   {:url "https://github.com/Myzel394/easytables.nvim" :config true}


  ; { :url "https://github.com/SidOfc/mkdx" }
    ; { :url "https://github.com/gabrielelana/vim-markdown" }
    ; { :url "https://github.com/plasticboy/vim-markdown" }
    ; { :url "https://github.com/vimwiki/vimwiki" }
   { :url "https://github.com/Scuilion/markdown-drawer"}
   { :url "https://github.com/masukomi/vim-markdown-folding"}
   { :url "https://github.com/tpope/vim-markdown"}

   { :url "https://github.com/atusy/tsnode-marker.nvim"}
   { :url "https://github.com/sirtaj/vim-openscad"}
   { :url "https://github.com/aklt/plantuml-syntax"}
   { :url "https://tildegit.org/sloum/gemini-vim-syntax"}
   ; { :url "https://github.com/vim-pandoc/vim-rmarkdown" }
   {:url "https://github.com/towolf/vim-helm"}


   ; ==> Color Scheme

   ; { :url "https://github.com/lifepillar/vim-gruvbox8" }

   {:url "https://github.com/ellisonleao/gruvbox.nvim"
    :config {:contrast :hard}
    :priority 1000}

   { :url "https://github.com/vim-scripts/peaksea"}
   { :url "https://github.com/wdhg/dragon-energy"}
   { :url "https://github.com/eemed/sitruuna.vim"}
   { :url "https://github.com/patstockwell/vim-monokai-tasty"}
   { :url "https://github.com/rafalbromirski/vim-aurora"}
   { :url "https://github.com/camilotorresf/vim-colors-yellownote"}
   { :url "https://github.com/ErichDonGubler/vim-sublime-monokai"}
   { :url "https://github.com/therubymug/vim-pyte"}
   { :url "https://github.com/noahfrederick/vim-noctu"}
   { :url "https://github.com/jeffkreeftmeijer/vim-dim"}
   { :url "https://github.com/scysta/pink-panic.nvim"}])

; { :url "https://github.com/vim-scripts/gruvbox"}
; (set vim.g.gruvbox_contrast "hard")
; (set vim.g.gruvbox_italicize_strings 1)
; (vim.cmd.colorscheme "gruvbox")


(set vim.o.background :dark)
(vim.cmd.colorscheme :gruvbox)



; (vim.highlight.create "Normal" { :guibg nil :ctermbg "NONE" })
; (vim.highlight.create "NormalFloat" { :guibg nil :ctermbg "NONE" })
(vim.api.nvim_set_hl 0 "normal" {:bg "none"})
(vim.api.nvim_set_hl 0 "normalFloat" {:bg "none"})
