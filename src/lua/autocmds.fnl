(local vapi vim.api)

;; Save .nvim.fnl to .nvim.lua
(vapi.nvim_create_autocmd
  [:BufWritePost]
  {:pattern [".nvim.fnl"]
   :callback
   (lambda [ev]
     (vim.cmd "!fennel --compile .nvim.fnl > .nvim.lua"))})

(vapi.nvim_create_autocmd
  [:BufEnter]
  {:pattern ["*.slint"]
   :callback
   (lambda [] (set vim.opt_local.filetype :slint))})
