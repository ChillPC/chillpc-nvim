(local kset vim.keymap.set)
(local vdiag vim.diagnostic)
(local lspbuf vim.lsp.buf)

(fn on_attach [client bufnr]
    (let [opts {:buffer bufnr :remap false :silent true}]
      (kset "n" "[l" vdiag.goto_prev { :desc " lsp"})
      (kset "n" "]l" vdiag.goto_next { :desc " lsp"})
      (kset "n" "gl" vdiag.open_float { :desc "Diagnostic lsp"})

      (kset "n" "gK" lspbuf.hover { :desc "Hover info lsp"})
      (kset "n" "g<leader>" lspbuf.hover { :desc "Hover info lsp"})
      (kset "n" "gi" lspbuf.implementation { :desc "Implementation lsp"})
      (kset "n" "gs" lspbuf.signature_help { :desc "Signature lsp"})
      (kset "n" "<leader>lwa" lspbuf.add_workspace_folder { :desc "Add workspace"})
      (kset "n" "<leader>lwr" lspbuf.remove_workspace_folder
                      { :desc "Delete workspace"})
      (kset
        "n" "<leader>lwl"
        (lambda []
          (print (vim.inspect (lspbuf.list_workspace_folders))))
        { :desc "List workspace"})
      (kset "n" "<leader>ld" lspbuf.type_definition { :desc "Type definition"})
      (kset "n" "<leader>la" lspbuf.code_action { :desc "Actions"})

      (kset "n" "gr" lspbuf.rename { :desc "Rename lsp"})

      (kset "n" "gd" lspbuf.definition { :desc "Definition lsp"})

      (if (pcall
           (lambda []
             (let [fzf (require "fzf-lua")]
              (kset "n" "ga" fzf.lsp_code_actions {:desc "Lsp Action"})
              (kset "n" "gL" fzf.diagnostics_document { :desc "Diagnostic lsp"})
              (kset "n" "gt" fzf.lsp_typedefs { :desc "Type definition"})
              (kset "n" "gD" fzf.lsp_declarations { :desc "Declaration lsp"})
              ; (kset "n" "gd" fzf.lsp_definitions { :desc "Definition lsp"})
              (kset "n" "g/" fzf.lsp_references { :desc "References lsp"}))))
        (do)
        ((kset "n" "ga" lspbuf.code_action { :desc "Actions"})
         (kset "n" "gt" lspbuf.type_definition { :desc "Type definition"})
         (kset "n" "gD" lspbuf.declaration { :desc "Declaration lsp"})
         (kset "n" "g/" lspbuf.references { :desc "References lsp"})))
          
      (let [lsp-format (lambda [] (lspbuf.format { :async true}))]
        (kset "n" "=l" lsp-format { :desc "Format lsp"})
        (kset "n" "g=" lsp-format { :desc "Format lsp"})
        (kset "n" "<leader>l=" lsp-format { :desc "Format lsp"}))))

{:on_attach on_attach}
