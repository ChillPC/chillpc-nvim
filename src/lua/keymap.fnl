; TODO : virtualedit all

(local kset vim.keymap.set)
(local cmd vim.cmd)
(local g vim.g)
(local o vim.o)
(local vbo vim.bo)
(local vwo vim.wo)
(local vfn vim.fn)
(local vapi vim.api)

; +--------------------------------------------------------
; | General
; +--------------------------------------------------------

(set g.transparency true)

(set g.mapleader " ")
(set g.maplocalleader :<C-c>)

(kset :n :<leader><leader>r (lambda [] (cmd.source :.nvim.lua)) {:desc "Source local"})
(kset :n :<leader><leader>R (lambda [] (cmd.source :$MYVIMRC)) {:desc "Source all"})
(kset :n :<leader><leader>E (lambda [] (cmd.edit :.nvim.lua)) {:desc "Edit local lua conf"})
(kset :n :<leader><leader>e (lambda [] (cmd.edit :.nvim.fnl)) {:desc "Edit local conf"})

(kset [:n :v] ";" ":")
(kset [:n :v] ":" ";")

(kset :n :Q "@")

(kset :n :<leader>! ":!!<Cr>")

(kset :n :<leader><leader>uv
      (lambda [] (cmd.source {:args ["~/.config/nvim/init.lua"]}))
      {:desc "Vim source"})

; --- Escape

((fn [keys] (each [_ key (ipairs keys)] (kset :i key :<Esc>)))
 [:jk :jK :Jk :Jk :kj :Kj :kJ :kJ])

; --- Session

(kset :n :<leader>qq (lambda [] (cmd.mksession {:bang true})) {:desc "Save session"})
(kset :n :<leader>q<Cr> (lambda [] (cmd.source {:args [:Session.vim]})) {:desc "Source session"})

; +--------------------------------------------------------
; | Terminal
; +--------------------------------------------------------

(kset :t :<C-q> "<C-\\><C-n>")
(kset :t ";;" "<C-\\><C-n>:")

(kset :n :<C-q> :<nop>)

(kset :t :<C-w> :<C-\\><C-n><C-w>)

(kset :n :<C-w>t cmd.terminal {:desc "Terminal"})
(kset :n :<C-w><Cr> cmd.terminal {:desc "Terminal"})

(kset :t :<C-Space> :<C-\\><C-n><leader>)

(fn clear-term []
  (set o.scrollback 500)
  (set o.scrollback 10000))

(kset :t :<C-l><C-l> clear-term {:desc "Clear term"})

(kset :n :<C-w>y 
    (lambda []
        (cmd "%yank z")
        (cmd.tabnew)
        (cmd "put z"))
  {:desc "Paste new tab"})

(kset :n :<leader><C-l> clear-term {:desc "Clear term"})


; +--------------------------------------------------------
; | Binary
; +--------------------------------------------------------

(kset :n :<leader>xx (lambda [] (set vbo.filetype "xxd")) {:desc "Xxd filtype"})
(kset :n :<leader>xc (lambda [] (cmd ":%!xxd -r")) {:desc :Compile})
(kset :n :<leader>xd (lambda [] (cmd ":%!xxd")) {:desc :Decompile})


; +--------------------------------------------------------
; | Writing
; +--------------------------------------------------------

((fn [chars] (each [_ c (ipairs chars)]
               (kset :i c (.. c :<C-g>u))))
 ["." "!" "?" ":" ";" "(" ")" "[" "]" "{" "}" "-"])

(kset :v "gm" ":norm" {:desc "nor[m]"})

(kset :v "<leader>s" (lambda [] (cmd.sort)) {:desc "Sort"})

; --- Search
(kset :n :<C-s> ":%s/")
(kset :v :<C-s> ":s/")

(kset :n :<C-g> ":%g/")
(kset :v :<C-g> ":g/")

; --- Format
(kset :n :== "m`gg=G``" {:desc "Format all"})
(kset :n :<leader>== "m`gg=G``" {:desc "Format all"})
(kset :n :<leader>=j (lambda [] (cmd {:args [:%!jq] })) {:desc "JSON Format"})
(kset :v :<leader>=j (lambda [] (cmd {:args ["'<,'>!jq"] })) {:desc "JSON Format"})
(kset :n :=p (lambda [] (cmd "!pre-commit run --all")) {:desc "Pre-commit all"})

; TODO
(kset :n :go "m`o<Esc>``" {:desc :o})
(kset :n :gO "m`O<Esc>``" {:desc :O})
; (kset :n "oo" "m`o<Esc>``")
; (kset :n "OO" "m`O<Esc>``")

; --- Indent
(kset :n :<leader>z<tab>2
      (lambda [] (set o.shiftwidth 2) (set o.softtabstop 2) (set o.tabstop 2))
      {:desc "Indent 2 spaces"})
(kset :n :<leader>z<tab>4
      (lambda [] (set o.shiftwidth 4) (set o.softtabstop 4) (set o.tabstop 4))
      {:desc "Indent 4 spaces"})

(kset :n :<leader>z<tab><tab>
      (lambda [] (set o.expandtab (not o.expandtab))
        (print (.. "Expandtab: " (tostring o.expandtab))))
      {:desc "Expandtab switch"})


; --- Move selection
(kset :v :<C-j> ":m '>+1<CR>gv=gv")
(kset :v :<C-k> ":m '<-2<CR>gv=gv")

; --- Yank/delete
(kset :n :<leader>y "\"+yy") 
; (kset :i "<leader>y" "<Esc>\"+yya") 
(kset :v :<leader>y "\"+y") 
(kset :n :<C-p> "\"+p")

(kset :n :dD :^D)
(kset :n :cC :^C)
(kset :n :Y :y$)
; (kset :v :p "\"_p")
(kset :v :p :P {:remap false})
(kset :v :P :p {:remap false})

; +--------------------------------------------------------
; | Text Display
; +--------------------------------------------------------

; --- Spell
(kset :n :<leader><leader>ss
      (lambda [] (do (set vwo.spell (not vwo.spell))
                   (print (.. "Spell: " (tostring vwo.spell)))))
      {:desc "Spellcheck switch " })

(kset :n :<leader><leader>sf (lambda [] (set vbo.spelllang :fr_FR))
      {:desc "fr_FR" })
(kset :n :<leader><leader>se (lambda [] (set vbo.spelllang :en_US))
      {:desc "en_US" })


; --- Fold ----------------------------------------------------

(kset :n :z<Cr> :zMzvzz {:desc "Fold around"})

(kset :n :<leader>zf (lambda [] (if (= o.foldmethod :syntax)
                                    (set o.foldmethod :indent)
                                    (set o.foldmethod :syntax))
                         (print (.. "Foldmethod: " o.foldmethod)))
      {:desc "Fold method switch"})

(for [i 0 9]
  (kset :n (.. :z (tostring i))
        (lambda [] (set o.foldlevel i)) {:desc (.. :Foldlevel= (tostring i)) })
  (kset :n (.. :<leader>z (tostring i))
        (lambda [] (set o.foldlevel i)) {:desc (.. :Foldlevel= (tostring i)) }))

; --- Style

(kset :n :<leader>zw (lambda [] (set o.wrap (not o.wrap))
                         (print (.. "Wrap: " (tostring o.wrap))))
      {:desc "Wrap switch" })

(kset :n :<leader>zc (lambda [] (if (= o.conceallevel 0)
                                    (set o.conceallevel 2)
                                    (set o.conceallevel 0))
                         (print (.. "Conceal level: " (tostring o.conceallevel))))
      {:desc "Conceal switch" })

(let [switch-hlsearch (lambda [] (set o.hlsearch (not o.hlsearch))
                        (print (.. "HL search: " (tostring o.hlsearch))))]
  (kset :n :<leader>z<Cr> switch-hlsearch {:desc "HL Search"}))

; --- Interface

(kset :n :<leader>z- (lambda [] (set vwo.cursorline (not vwo.cursorline)))
      {:desc "Cursor -" })
(kset :n :<leader>z| (lambda [] (set vwo.cursorcolumn (not vwo.cursorcolumn)))
      {:desc "Cursor |" })
(kset :n :<leader>z+ (lambda [] (set vwo.cursorcolumn (not vwo.cursorcolumn))
                         (set vwo.cursorline vwo.cursorcolumn))
      {:desc "Cursor +" })

(kset :n :<leader>zn
      (lambda []
        (match [o.number o.relativenumber]
          [false _n] (do (set o.number true) (set o.relativenumber true))
          [true true] (set o.relativenumber false)
          [true false] (set o.number false)))
      {:desc "Cycle number"})

(kset :n :<leader>zt
      (lambda []
        (do (if g.transparency
              (set o.background :dark)
              (do (vapi.nvim_set_hl 0 :normal {:bg :none})
                (vapi.nvim_set_hl 0 :normalFloat {:bg :none})))
          (set g.transparency (not g.transparency))))
      {:desc "Transparency switch" })

(kset :n :<leader>zs (lambda [] (if (= o.background :dark)
                                    (set o.background :light)
                                    (set o.background :dark)))
      {:desc "Light switch" })

; TODO
; (kset :n "<leader>zb :call" "Toggle_transparent_background()<cr>" {:silent true })


; if has_key(plugs, "vim-lsp")
;     (kset :n "<leader>l=" ":LspDocumentFormat<Cr>")
; endif

; +--------------------------------------------------------
; | Navigating
; +--------------------------------------------------------

; set whichwrap+=<,>,h,l
; set backspace=indent,eol,start  " Makes backspace key more powerful.)

; --- Text

(kset [:n :v] :<down> :gj {:noremap true})
(kset [:n :v] :<up> :gk {:noremap true})
(kset [:n :v] :j :gj {:noremap true})
(kset [:n :v] :k :gk {:noremap true})
(kset [:n :v] :gj :j {:noremap true})
(kset [:n :v] :gk :k {:noremap true})
(kset [:n :v] :yj :yj {:noremap true})
(kset [:n :v] :yk :yk {:noremap true})
(kset [:n :v] :dj :dj {:noremap true})
(kset [:n :v] :dk :dk {:noremap true})

(kset [:n :v :o] :H :^zz)
(kset [:n :v :o] :L :$)
(kset [:n :v :o] :K "{")
(kset [:n :v :o] :J "}")

(kset [:n :v] :N "m`Nzzzv")
(kset [:n :v] :n "m`nzzzv")

(kset :n :* (lambda [] (set o.hlsearch true)
                (vfn.feedkeys "m`*``" :n)))
(kset :n :# (lambda [] (set o.hlsearch true)
                (vfn.feedkeys "m`#``" :n)))

(kset :n :<C-u> :<C-u>zz)
(kset :n :<C-d> :<C-d>zz)
(kset :i :<C-u> :<Esc><C-u>)
(kset :i :<C-d> :<Esc><C-d>)

(kset :n :<C-m> "mzJ`z")

; (kset :n "<leader>w" "<C-w>" {:noremap false :desc "window" })
(kset [:i :t] :<C-w> "<C-\\><C-n><Esc><C-w>" {:noremap true :desc :<C-w>})

; --- Panes

; Navigate
(kset [:n :t] :<C-j> "<Esc><C-\\><C-n><c-W><Cr><C-W>j" {:desc "Go "})
(kset [:n :t] :<C-k> "<Esc><C-\\><C-n><c-W><Cr><C-W>k" {:desc "Go "})
(kset [:n :t] :<C-h> "<Esc><C-\\><C-n><c-W><Cr><C-W>h" {:desc "Go "})
(kset [:n :t] :<C-l> "<Esc><C-\\><C-n><c-W><Cr><C-W>l" {:desc "Go "})

; Move
(kset [:n :i :t] :<C-W><C-j> "<C-\\><C-n><C-W>J" {:desc "Move "})
(kset [:n :i :t] :<C-W><C-h> "<C-\\><C-n><C-W>H" {:desc "Move "})
(kset [:n :i :t] :<C-W><C-k> "<C-\\><C-n><C-W>K" {:desc "Move "})
(kset [:n :i :t] :<C-W><C-l> "<C-\\><C-n><C-W>L" {:desc "Move "})

; Create windows
; Split Vertical
(kset [:n :i :t] :<C-W>b "<Esc><C-\\><C-n><C-W>v" {:desc ""})
(kset [:n :i :t] :<C-W><C-b> "<Esc><C-\\><C-n><C-W>v" {:desc ""})

; Split Horizontal
(kset [:n :i :t] :<C-W>n "<Esc><C-\\><C-n><C-W>s" {:desc ""})
(kset [:n :i :t] :<C-W><C-n> "<Esc><C-\\><C-n><C-W>s" {:desc ""})

; Close pane
(kset [:n :i :t] :<C-W><C-x> "<Esc><C-\\><C-n><C-W>q" {:desc ""})

; Resize
(kset [:n :i :t] "<C-W><C-\\>" (lambda []
                                    (vapi.nvim_win_set_width 0 0))
      {:desc "Resize "})
(kset [:n :i :t] :<C-W><C-_> (lambda [] (cmd.resize {:args [:0]}))
      {:desc "Resize "})


(kset [:n :i :t] :<C-W>K (lambda [] (cmd.resize {:args ["+20"]}))
      {:desc "Resize  +20"})
(kset [:n :i :t] :<C-W>+ (lambda [] (cmd.resize {:args ["+10"]}))
      {:desc "Resize  +10"})

(kset [:n :i :t] :<C-W>J (lambda [] (cmd.resize {:args ["-20"]}))
      {:desc "Resize  -20"})
(kset [:n :i :t] :<C-W>- (lambda [] (cmd.resize {:args ["-10"]}))
      {:desc "Resize  -10"})

(kset [:n :i :t] :<C-W>L (lambda [] 
                               (vapi.nvim_win_set_width 0 (+ (vapi.nvim_win_get_width 0) 20)))
      {:desc "Resize  +20"})
(kset [:n :i :t] :<C-W>> (lambda [] 
                               (vapi.nvim_win_set_width 0 (+ (vapi.nvim_win_get_width 0) 10)))
      {:desc "Resize  +10"})

(kset [:n :i :t] :<C-W>L (lambda []
                               (vapi.nvim_win_set_width 0 (- (vapi.nvim_win_get_width 0) 20)))
      {:desc "Resize  -20"})
(kset [:n :i :t] :<C-W>< (lambda []
                               (vapi.nvim_win_set_width 0 (- (vapi.nvim_win_get_width 0) 10)))
      {:desc "Resize  -10"})

; --- Tabs

; Navigate
(kset [:n :i :t] :<C-w>l cmd.tabnext {:desc " Tab"})
(kset [:n :i :t] :<C-w>h cmd.tabprevious {:desc " Tab"})

; Navigate number
(for [i 1 10]
  (kset [:n :i :t] (.. :<C-w> (tostring i))
        (lambda [] (cmd.tabnext {:args [(tostring (if (= i 10) 0 i))]}))
        {:desc (.. " " (tostring i))}))

; Create
(kset :n :<C-W>w cmd.enew {:desc "New buffer"})

(kset [:n :t] :<C-w>g (lambda [] (cmd "tab split")) {:desc "New tab"})
(kset [:n :t] :<C-w><C-g> (lambda [] (cmd "tab split")) {:desc "New tab"})
(kset [:n :t] :<C-w><leader> (lambda [] (cmd "tab split")) {:desc "New tab"})

; Close
(kset [:n :i :t] "<C-W>x" (lambda [] (cmd :tabclose)) {:desc ""})

; Move
(kset :n :<C-w>j (lambda [] (cmd.tabmove "-1")) {:desc " Move"})
(kset :n :<C-w>k (lambda [] (cmd.tabmove "+1")) {:desc " Move"})

; +--------------------------------------------------------
; | Diff
; +--------------------------------------------------------

(kset :n :<leader>s<Cr> ":diffthis<Cr>" {:desc "Diff this"})
(kset :n :<leader>su ":diffupdate<Cr>" {:desc "Diff update"})
(kset :n :<leader>sb ":diffsplit" {:desc "Diff split"})
(kset :n :<leader>sp ":diffpatch<Cr>" {:desc "Diff patch"})
(kset :n :<leader>sP ":diffpatch" {:desc "Diff patch"})
(kset :n :<leader>sx ":diffoff<Cr>" {:desc "Diff off"})

; +--------------------------------------------------------
; | Special windows
; +--------------------------------------------------------

(kset :n :<C-w>m cmd.messages {:desc "Messages"})

; +--------------------------------------------------------
; | Cd/Pwd
; +--------------------------------------------------------

; --- PWD

(kset :n :<leader>c<leader> cmd.pwd {:desc :pwd})
(kset :n :<leader>c% (lambda [] (print (vfn.expand "%:p"))) {:desc :Filepath})
(kset :n :<leader>% (lambda [] (print (vfn.expand "%:p"))) {:desc :Filepath})


; --- Relative
(kset :n :<leader>c<Cr> (lambda [] (cmd.lcd {:args [ "%:h"]})
                            (cmd.pwd))
      {:desc "lcd %"})

(kset :n :<leader><leader>c<Cr> (lambda [] (cmd.cd {:args [ "%:h"]})
                                    (cmd.pwd))
      {:desc "cd %"})

(kset :n :<leader>c<Tab>
      (lambda [] (do (cmd.lcd :..) (cmd.pwd)))
      {:desc "lcd -1"})

(kset :n :<leader><leader>c<Tab>
      (lambda [] (do (cmd.cd :..) (cmd.pwd)))
      {:desc "cd -1"})

(local cding (icollect [number _ (ipairs [1 2 3 4 5 6 7 8 9])]
                       (do (lambda loop [n acc]
                             (if (<= n 0) acc (loop (- n 1) (.. acc "../"))))
                         (loop number ""))))

(each [number command (ipairs cding)]
  (kset :n (.. :<leader>c (tostring number))
        (lambda [] (do (cmd.lcd {:args [command]})
                     (cmd.pwd)))
        {:desc (.. "lcd -" (tostring number))})

  (kset :n (.. :<leader><leader>c (tostring number))
        (lambda [] (do (cmd.cd {:args [command]})
                     (cmd.pwd)))
        {:desc (.. "cd -" (tostring number))}))


; Rifle
(kset :n :<leader><leader><Cr>
      (lambda [] (os.execute (.. "rifle '" (vfn.expand "%") "'")))
      {:desc "Rifle"})

; --- Letter pathes

(for [i (string.byte :A) (string.byte :z)]
  (let [c (string.char i)
        env-path (os.getenv (.. c "_PATH"))]
    (when env-path
      (kset :n (.. :<leader><leader>c c)
            (lambda [] (cmd.cd {:args [env-path]})
              (cmd.pwd))
            {:desc (.. "cd " env-path)})

      (kset :n (.. :<leader>c c)
            (lambda [] (cmd.lcd {:args [env-path]})
              (cmd.pwd))
            {:desc (.. "lcd " env-path)}))))

(kset
  :n :<Leader>C
  (fn []
    (let [curbufnr (vapi.nvim_get_current_buf)
          buflist (vapi.nvim_list_bufs)]
      (each [_ bufnr (ipairs buflist)]
        (when (and (and (. (. vbo bufnr) :buflisted)
                        (not= bufnr curbufnr))
                   (not= (vfn.getbufvar bufnr :bufpersist) 1))
          (cmd (.. "bd! " (tostring bufnr)))))))
  {:desc "Close unused buffers" :silent true})
