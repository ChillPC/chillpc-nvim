(vim.api.nvim_create_autocmd
  ["FileType"]
  {:pattern :markdown
   :callback
   (lambda []
     (set vim.o.softtabstop 4)
     (set vim.o.shiftwidth 4))})

