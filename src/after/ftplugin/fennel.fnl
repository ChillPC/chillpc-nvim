; (vim.api.nvim_create_autocmd
;   ["FileType"]
;   {:pattern :fennel
;    :callback
;    (lambda [])})

(vim.keymap.set
  :v :<leader>r<Cr>
  ":!antifennel -<Cr>"
  { :desc "Lua -> Fennel"})

(vim.keymap.set
  :n :<leader>r<Cr>
  "vip:!antifennel -<Cr>"
  { :desc "Lua -> Fennel"})

(vim.keymap.set
  :v :<leader>rl
  ":!fennel -c /dev/stdin<Cr>"
  { :desc "Fennel -> Lua"})

(vim.keymap.set
  :n :<leader>rl
  "vip:!fennel -c /dev/stdin<Cr>"
  { :desc "Fennel -> Lua"})
