(local g vim.g)
(local vcmd vim.cmd)
(local kset vim.keymap.set)

(pcall
  (lambda []
    ((. (require "nvim-autopairs") :setup)
     {:check_ts true  ; Deactivate if not good
      ; :disable_filetype ["scheme" "racket" "guile" "clojure" "cljs" "lisp"]
      :enable_check_bracket_line false
      :fast_wrap
      {:map "<C-s>"
       :end_key "$"
       :keys "qwertyuiopzxcvbnmasdfghjkl"
       :check_comma true
       :highlight "Search"
       :highlight_grey "Comment"
       :pattern "[%[%(%{%<%)%>%]%)%}%'%\"%,%.%:%;%=%-% ]"
       ; :pattern "[=[[%'%\"%)%>%]%)%}%,]]=]"
       :chars [ "{" "[" "(" "\"" "*"]}})))


(pcall
  (lambda []
    (kset :n :<leader><Cr>Tg
          vcmd.ConjureSchemeGuile
          {:desc "Guile R6RS"})

    (kset :n :<leader><Cr>To
          vcmd.ConjureSchemeGosh
          {:desc "Gosh R7RS"})

    (kset :n :<leader><Cr>Tc
          vcmd.ConjureSchemeChibi
          {:desc "Chibi R7RS"})

    (kset :n :<leader><Cr>Tp
          vcmd.ConjureSchemePetite
          {:desc "Petite R6RS"})))


; (tset g "conjure#client#scheme#stdio#value_prefix_pattern" false)
