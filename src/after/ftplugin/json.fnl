(vim.keymap.set "n" "=j" (fn [] (vim.cmd "%!jq")) { :desc "JSON Format"})
(vim.keymap.set "v" "=j" (fn [] (vim.cmd "'<,'>!jq")) { :desc "JSON Format"})
