(vim.keymap.set
  "n" "<leader>pp"
  (lambda [] (vim.cmd "!setsid -f openscad %:p"))
  {:buffer true
   :desc "Openscad"})

(set vim.o.commentstring "/* %s */")
