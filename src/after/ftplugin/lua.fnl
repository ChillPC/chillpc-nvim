(vim.keymap.set
  :v :<leader>rf
  ":!antifennel -<Cr>"
  { :desc "Lua -> Fennel"})

(vim.keymap.set
  :n :<leader>rf
  "vip:!antifennel -<Cr>"
  { :desc "Lua -> Fennel"})

(vim.keymap.set
  :v :<leader>rF
  ":!fennel -c /dev/stdin<Cr>"
  { :desc "Fennel -> Lua"})

(vim.keymap.set
  :n :<leader>rF
  "vip:!fennel -c /dev/stdin<Cr>"
  { :desc "Fennel -> Lua"})
