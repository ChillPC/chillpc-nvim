(local js-based-languages [:typescript :javascript :typescriptreact :vue])

(pcall
  (lambda []
    (let [dap (require "dap")]
      (each [lang js-based-languages]
        (tset dap.adapters lang
               { :type "executable"
                :command (vim.fn.exepath "js-debug-adapter")})

        (tset dap.configurations lang
               [{ :name "Current"
                 :type "javascript" ; Link to adapter def
                 :request "launch"
                 :program "${file}"}

                { :name "Main"
                 :type "javascript"
                 :request "launch"
                 :program "${workspaceFolder}/main.js"}

                { :name "Debug"
                 :type "javascript"
                 :request "launch"
                 :justMyCode false
                 :program "${workspaceFolder}/debug.js"}

                { :name "Test"
                 :type "javascript"
                 :request "launch"
                 :program "${workspaceFolder}/test.py"}])))))
