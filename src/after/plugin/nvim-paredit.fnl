(pcall
  (lambda [] 
    ((. (require :nvim-paredit) :setup)
     {:filetypes [:clojure :racket :scheme :commonlisp :fennel]
      :indent
      {:enabled true
       :indentor (. (require :nvim-paredit.indentation.native) :indentor)}
        
      :extensions
      {:racket (require :nvim-paredit-scheme.extension)}})))
