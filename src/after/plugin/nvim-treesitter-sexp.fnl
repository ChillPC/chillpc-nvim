
(pcall
  (lambda []
    ((. (require :treesitter-sexp) :setup)
     {:enabled true
      :keymaps
      {:commands {:barf_left ">("
                  :barf_right "<)"
                  :insert_head :<I
                  :insert_tail :>I
                  :promote_elem :<LocalLeader>O
                  :promote_form :<LocalLeader>o
                  :slurp_left "<("
                  :slurp_right ">)"
                  :splice "<LocalLeader>@"
                  :swap_next_elem :>e
                  :swap_next_form :>f
                  :swap_prev_elem :<e
                  :swap_prev_form :<f}
       :motions {:form_end ")"
                 :form_start "("
                 :next_elem "]e"
                 :next_elem_end "]E"
                 :next_top_level "]]"
                 :prev_elem "[e"
                 :prev_elem_end "[E"
                 :prev_top_level "[["}
       :textobjects {:inner_elem :ie
                     :inner_form :if
                     :inner_top_level :iF
                     :outer_elem :ae
                     :outer_form :af
                     :outer_top_level :aF}}
      :set_cursor true})))

