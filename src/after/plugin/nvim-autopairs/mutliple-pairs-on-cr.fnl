; (local autopairs (require :nvim-autopairs))

; (local Rule (require :nvim-autopairs.rule))

; (local cond (require :nvim-autopairs.conds))


; (fn get-closing-for-line [line]
;   (let [i (- 1)]
;     (var clo "")
;     (while true
;       (set-forcibly! (i _) (string.find line "[%(%)%{%}%[%]]" (+ i 1)))
;       (when (= i nil) (lua :break))
;       (local ch (string.sub line i i))
;       (local st (string.sub clo 1 1))
;       (if (= ch "{") (set clo (.. "}" clo)) (= ch "}")
;         (do
;           (when (not= st "}") (lua "return \"\""))
;           (set clo (string.sub clo 2))) (= ch "(")
;         (set clo (.. ")" clo)) (= ch ")")
;         (do
;           (when (not= st ")") (lua "return \"\""))
;           (set clo (string.sub clo 2))) (= ch "[")
;         (set clo (.. "]" clo)) (= ch "]")
;         (do
;           (when (not= st "]") (lua "return \"\""))
;           (set clo (string.sub clo 2)))))
;     clo))

; (autopairs.remove_rule "(")

; (autopairs.remove_rule "{")

; (autopairs.remove_rule "[")

; (autopairs.add_rule (: (: (: (Rule "[%(%{%[]" "") :use_regex true)
;                           :replace_endpair
;                           (fn [opts] (get-closing-for-line opts.line)))
;                        :end_wise
;                        (fn [opts] (not= (get-closing-for-line opts.line) ""))))

