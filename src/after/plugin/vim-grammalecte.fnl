(vim.keymap.set "n" "<leader><leader>sFF" vim.cmd.GrammalecteCheck { :desc "Grammalecte"})
(vim.keymap.set "n" "<leader><leader>sFf" vim.cmd.GrammalecteCheck { :desc "Grammalecte"})
(vim.keymap.set "n" "<leader><leader>sF<Cr>" vim.cmd.GrammalecteCheck { :desc "Grammalecte"})
(vim.keymap.set "n" "<leader><leader>sFx" vim.cmd.GrammalecteClear { :desc "Clear"})

(set vim.g.grammalecte_cli_py (vim.fn.exepath "grammalecte-cli.py"))
