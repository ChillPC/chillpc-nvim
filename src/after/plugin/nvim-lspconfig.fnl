(local lspconfig (require :lspconfig))

; (vim.tbl_deep_extend
;   :keep lspconfig
;   {:guile_lsp_server
;    {:name :scheme-lsp-server
;     :filetypes ["guile" "scheme"]
;     :cmd ["guile-lsp-server" "--stdio"]}})

; (lspconfig.scheme_lsp_server.setup)

; (vim.lsp.start
;   {:name :scheme-lsp-server
;    :filetypes ["guile" "scheme"]
;    :cmd ["guile-lsp-server" "--stdio"]})


(vim.api.nvim_create_autocmd
 :FileType
 {:pattern [:scheme :guile]
  :callback
  (fn [ev]
    (vim.lsp.start
      {:cmd ["guile-lsp-server"]
       :name :guile-lsp-server}))})
