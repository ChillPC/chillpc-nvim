(local g vim.g)

(pcall
  (lambda []
    (let [rainbow-delimiters (require :rainbow-delimiters)]
      (set g.rainbow_delimiters
           {:highlight [:RainbowDelimiterRed
                        :RainbowDelimiterYellow
                        :RainbowDelimiterBlue
                        :RainbowDelimiterOrange
                        :RainbowDelimiterGreen
                        :RainbowDelimiterViolet
                        :RainbowDelimiterCyan]
            :query {"" :rainbow-delimiters :lua :rainbow-blocks}
            :strategy {"" (. rainbow-delimiters.strategy :global)
                       :vim (. rainbow-delimiters.strategy :local)}}))))
