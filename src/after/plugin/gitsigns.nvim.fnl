(local kset vim.keymap.set)

(pcall (lambda []
         (let [gs (require "gitsigns")]
           (kset [ "n" "v" ] "<leader>hh" gs.reset_hunk { :desc "Reset"})
           (kset "n" "<leader>hj" gs.next_hunk { :desc ""})
           (kset "n" "<leader>hk" gs.prev_hunk { :desc ""})

           (kset "n" "[h" gs.prev_hunk { :desc "Previous hunk"})
           (kset "n" "]h" gs.next_hunk { :desc "Next hunk"})

           (kset "n" "<leader>h<leader>" gs.preview_hunk { :desc "Preview"})
           (kset [ "n" "v" ] "<leader>h<Cr>" gs.stage_hunk { :desc "Stage"})
           (kset "n" "<leader>hu" gs.undo_stage_hunk { :desc "Undo"})
           (kset "n" "<leader>hR" gs.reset_buffer { :desc "Reset all"})
           ; (kset "n" "<leader>h!!" gs.toggle_current_line_blame { :desc "Blame"})
           (kset "n" "<leader>h!" (fn [] (gs.blame_line {:full true})) { :desc "Blame all"})

           (kset "n" "<leader>hs" gs.diffthis { :desc "Diff"})

           (kset "n" "<leader>hy" gs.toggle_deleted { :desc "Toggle"}))))

;(vim.keymap('n', '<leader>hD', function() gs.diffthis('~') end)
