(local kset vim.keymap.set)
(local vcmd vim.cmd)
(local vg vim.g)
(local vapi vim.api)

(local constants (require :constants))

(kset :n :<leader>zp vcmd.ParinferToggle {:desc "Paringer toggle"})

(kset
  :n :<leader>zP
  (lambda [] (vcmd.ParinferToggle {:bang true}))
  {:desc "Paringer toggle all"})

(set vg.parinfer_no_maps false)

(vapi.nvim_create_autocmd
  :FileType
  {:pattern constants.ft-collections.lisp
   :callback
   (lambda [])})
     ; (kset :i :>> "<Plug>(parinfer-tab)" {:buffer true})
     ; (kset :i :<< "<Plug>(parinfer-backtab)" {:buffer true})

     ; (kset :n ">>" "a<Plug>(parinfer-tab)<Esc>" {:buffer true})
     ; (kset :n "<<" "a<Plug>(parinfer-backtab)<Esc>" {:buffer true}))})
