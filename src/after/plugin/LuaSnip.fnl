(local luasnip (require "luasnip"))
(local from_lua (require :luasnip.loaders.from_lua))

(vim.keymap.set ["i" "s"] "<Tab>"
                (lambda [] (if (luasnip.expand_or_jumpable)
                             (luasnip.expand_or_jump)
                             (vim.api.nvim_feedkeys
                               (vim.api.nvim_replace_termcodes "<Tab>" true false true)
                               "n" false))) { :noremap true})


(vim.keymap.set ["i" "s"] "<S-Tab>"
                (lambda [] (luasnip.jump -1))
                { :noremap true})


(vim.keymap.set "n" "<leader><leader>us" (lambda []
                                           (from_lua.load {:paths "~/.config/nvim/luasnip"}))
                { :desc "Luasnip source" })

(from_lua.load {:paths "~/.config/nvim/luasnip"})
