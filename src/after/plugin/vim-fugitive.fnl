(local kset vim.keymap.set)
(local vcmd vim.cmd)

(kset "n" "<leader>ga"
                (lambda [] (vcmd
                             (.. "Git add \""
                                 (vim.fn.expand "%:p") "\"")))
                { :desc "Add"})
(kset "n" "<leader>gl"  (lambda [] (vcmd.Gvdiffsplit)) { :desc "Diff split"})
(kset "n" "<leader>gL"  (lambda [] (vcmd.Gvdiffsplit {:bang true})) { :desc "Diff split!"})
(kset "n" "<leader>gm"  (lambda [] (vcmd.Git! ["mergetool"])) { :desc "Mergetool"})
(kset "n" "<leader>gg"  (lambda [] (vcmd.G)) { :desc "Git"})
(kset "n" "<leader>gc"  (lambda [] (vcmd.Git { :args ["commit"]})) { :desc "Commit"})
(kset "n" "<leader>gC"  (lambda [] (vcmd.Git { :args ["commit -n"]})) { :desc "Commit -n"})
(kset "n" "<leader>gp"  (lambda [] (vcmd.Git { :args ["pull"]})) { :desc "Pull"})
(kset "n" "<leader>gP"  (lambda [] (vcmd.Git { :args ["push"]})) { :desc "Push"})
(kset "n" "<leader>g!"  (lambda [] (vcmd.Git { :args ["blame"]})) { :desc "Blame"})
(kset "n" "<leader>gd"  (lambda [] (vcmd.Git { :args ["whatchanged"]})) { :desc "Changes"})
(kset "n" "<leader>gh"  (lambda [] (vcmd.Git { :args ["switch"]} )) { :desc "Switch"})
(kset "n" "<leader>goo" (lambda [] (vcmd.Git { :args ["log"]})) { :desc "Log"})
(kset "n" "<leader>gos" (lambda [] (vcmd.Git { :args ["show"]})) { :desc "Show commit"})
(kset "n" "<leader>gob" (lambda [] (vcmd.Git { :args ["show-branch"]})) { :desc "Show branches"})
(kset "n" "<leader>gss" (lambda [] (vcmd.Gvdiffsplit { :args ["master"]})) { :desc "Diff master"})
(kset "n" "<leader>gw"  (lambda [] (vcmd "!setsid -f gitg")) { :silent true :desc "Gitg"})

(kset "n" "<leader>sh" ":diffget //2<Cr>" { :desc "Diff get 2"})
(kset "n" "<leader>sl" ":diffget //3<Cr>" { :desc "Diff get 3"})
