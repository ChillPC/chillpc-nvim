(local kset vim.keymap.set)
(local vcmd vim.cmd)
(local vg vim.g)


; (pcall
;   (lambda []))
;     ; ((. (require :colorizer) :setup) {})
(set vg.colorizer_disable_bufleave 1)
; (set vim.g.colorizer_use_virtual_text 1)

(kset :n "<leader>zr" vcmd.ColorToggle {:desc "Escape ansi"})
(kset :n "<leader>zR" (lambda [] (vcmd.ColorToggle) (vcmd.ColorToggle)) {:desc "Escape ansi"})
