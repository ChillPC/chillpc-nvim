((. (require "bufferline") :setup)
 { :options
  {:separator_style "slope"
   :mode "tabs"
   :diagnostics "nvim_lsp" 
   :numbers "buffer_id"
   :indicator {
       ; :icon '▎', -- this should be omitted if indicator style is not 'icon'
       :style "underline" ;'icon' | '' | 'none',
   }}})
