(local kset vim.keymap.set)
(local sign_define vim.fn.sign_define)


(pcall
  (lambda []
    (let [dap (require :dap)
          ui (require :dapui)
          dap-widgets (require :dap.ui.widgets)]

      (ui.setup
        {:controls {:element :repl
                    ; :enabled true
                    :icons {:disconnect ""
                            :pause ""
                            :play ""
                            :run_last ""
                            :step_back ""
                            :step_into ""
                            :step_out ""
                            :step_over ""
                            :terminate ""}}
         :icons {:collapsed ""
                 :current_frame ""
                 :expanded ""}})

      (sign_define :DapBreakpoint {:text "●" :texthl :LspDiagnosticsDefaultError})
      (sign_define :DapLogPoint {:text "◉" :texthl :LspDiagnosticsDefaultError})
      ; (sign_define "DapStopped" {:text "🞂" :texthl "LspDiagnosticsDefaultInformation" :linehl "CursorLine"})
      (sign_define :DapStopped {:text "→" :texthl :LspDiagnosticsDefaultInformation :linehl :CursorLine})
      (sign_define :DapBreakpointCondition {:text ""})

      (kset :n :<Leader>dd    (lambda [] (dap.toggle_breakpoint)) {:desc "Breakpoint"})
      (kset :n :<Leader>dc    (lambda [] (dap.clear_breakpoints)) {:desc "Clear all breaks"})

      (kset :n :<Leader>d<Cr> (lambda [] (dap.continue)) {:desc "Continue"})
      (kset :n :<Leader>de    (lambda [] (ui.eval)) {:desc "Eval"})

      (kset :n :<Leader>dK    (lambda [] (dap-widgets.hover)) {:desc "Hover"})
      (kset :n :<Leader>d<leader> (lambda [] (dap-widgets.hover)) {:desc "Hover"})

      (kset :n :<Leader>dp    (lambda [] (dap-widgets.preview)) {:desc "Preview"})

      (kset :n :<Leader>dh    (lambda [] (dap.step_back)) {:desc "<- Back"})
      (kset :n :<Leader>dj    (lambda [] (dap.step_into)) {:desc "v Into"})
      (kset :n :<Leader>dk    (lambda [] (dap.step_out)) {:desc "^ Out"})
      (kset :n :<Leader>dl    (lambda [] (dap.step_over)) {:desc "-> Over"})
      (kset :n :<Leader>dJ    (lambda [] (dap.run_to_cursor)) {:desc "Exec -> Cursor"})
      (kset :n :<Leader>dL    (lambda [] (dap.goto_)) {:desc "Goto cursor (often unsupported)"})

      (kset :n :<Leader>df dap.focus_frame {:desc "Focus on cursor"})

      (kset :n :<Leader>d+    (lambda [] (dap.up)) {:desc "Up in stack"})
      (kset :n :<Leader>d-    (lambda [] (dap.down)) {:desc "Down in stack"})

      (kset :n :<Leader>dx    (lambda [] (dap.terminate) (ui.close)) {:desc "Exit"})
      (kset :n :<Leader>dr    (lambda [] (dap.repl.toggle)) {:desc "Repl"})

      (kset :n :<Leader>dU    ui.update_render {:desc "Update render"})
      (kset :n :<Leader>du    ui.toggle {:desc "Toggle UI"})
      (kset :n :<Leader>dR    dap.run_last {:desc "Run last"})
      (kset :n :<Leader>dr    dap.restart {:desc "Restart"})

      (tset dap :listeners :before :event_initialized :dapui_config
            (lambda [] (ui.open {}))))))

  ; (tset dap :listeners :before :event_terminated :dapui_config (lambda []
  ; 	(ui.close {})
  ; ))

  ; (tset dap :listeners :before :event_exited :dapui_config (lambda []
  ; 	(ui.close {})
  ; ))
