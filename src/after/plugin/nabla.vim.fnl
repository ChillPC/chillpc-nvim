(local nabla (require "nabla"))

(vim.keymap.set
  "n" "<leader>k"
  (lambda [] (nabla.popup))
  { :desc "KaTeX preview"})
