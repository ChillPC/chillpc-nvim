(local python-path 
  (let [venv (os.getenv "VIRTUAL-ENV")]
    (if venv
      (.. venv "/bin/python")
      (vim.fn.exepath "python3"))))

(local pytest-path 
  (let [venv (os.getenv "VIRTUAL-ENV")]
    (if venv
      (.. venv "/bin/pytest")
      (vim.fn.exepath "pytest"))))

(pcall
  (lambda []
    (let [dap (require "dap")]
      (set dap.adapters.python
           {:type "executable"
            :command (vim.fn.exepath "debugpy-adapter")})

      (set dap.configurations.python 
           [{:name "Current"
             :type :python ; Link to adapter def
             :console :integratedTerminal
             :request :launch
             :program "${file}"
             :pythonPath python-path}

            {:name "Main"
             :type :python
             :console :integratedTerminal
             :request :launch
             :program "${workspaceFolder}/main.py"
             :pythonPath python-path}

            {:name "Debug"
             :type :python
             :console :integratedTerminal
             :request :launch
             :justMyCode false
             :program "${workspaceFolder}/debug.py"
             :pythonPath python-path}

            {:name "Pytest"
             :type :python
             :console :integratedTerminal
             :request :launch
             :module :pytest
             :justMyCode false
             :program "pytest"
             :args "${workspaceFolder}"
             :pythonPath python-path}

            {:name "Test"
             :type :python
             :console :integratedTerminal
             :request :launch
             :program "${workspaceFolder}/test.py"
             :pythonPath python-path}]))))
