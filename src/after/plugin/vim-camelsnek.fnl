(local kset vim.keymap.set)

(kset "v" "<leader>rs" ":Snek<Cr>" {:desc "Snake"})
(kset "v" "<leader>r_" ":Snek<Cr>" {:desc "Snake"})
(kset "v" "<leader>rS" ":Screm<Cr>" {:desc "SNAKE"})
(kset "v" "<leader>rc" ":CamelB<Cr>" {:desc "camel Back"})
(kset "v" "<leader>rC" ":Camel<Cr>" {:desc "Camel"})
(kset "v" "<leader>r-" ":Kebab<Cr>" {:desc "Kebab"})
