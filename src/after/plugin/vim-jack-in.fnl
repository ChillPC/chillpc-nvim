(local api vim.api)
(local kset vim.keymap.set)
(local cmd vim.cmd)

(api.nvim_create_autocmd
  :FileType
  {:pattern :clojure
   :callback
   (lambda []
     (kset
       :n :<leader><Cr>Ca (lambda [] (cmd.Clj "-M:lib/hotload:dev/reloaded:repl/rebel"))
       {:buffer true
        :desc "Start Clojure repl"})
     ; (kset
     ;   :n :<leader><Cr>x (lambda [] (cmd "!killall clj"))
     ;   {:buffer true :desc "Killall clj"})
     (kset
       :n :<leader><Cr>Cr
       (lambda []
         (cmd.ConjureEval
           "(require '[clojure.tools.namespace.repl :refer [refresh]]) (refresh)"))
       {:buffer true
        :desc "Cloj refresh"})

     (kset
       :n :<leader><Cr>Ct
       (lambda []
         (cmd.ConjureEval
           "(require '[clojure.test :refer [run-tests]]) (run-tests)"))
       {:buffer true
        :desc "Cloj tests"}))})
