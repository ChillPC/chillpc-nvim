(local vcmd vim.cmd)
(local kset vim.keymap.set)

(pcall
  (lambda []
    ((. (require "colorizer") :setup))

    (kset :n "<leader>zC"
          (lambda [] (vcmd.ColorizerToggle))
          { :desc "Color toggle"})))
