(pcall
  (lambda []
    (let [formatter (require "formatter")]

      (formatter.setup
        { :filetype
        { :* [(require "formatter.filetypes.any")
              (let [any (require "formatter.filetypes.any")] any.remove_trailing_whitespace) ]
        :ocaml [ (. (require "formatter.filetypes.ocaml") :ocamlformat) ]

        }
        :logging true
        :log_level vim.log.levels.WARN
        })

      (vim.keymap.set "n" "=f" (fn [] (vim.cmd.Format)) { :desc "Formater"}))))
