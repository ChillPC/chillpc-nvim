(local cmp (require :cmp))
(local luasnip (require :luasnip))
; (local cmp_autopairs (require :nvim-autopairs.completion.cmp))

(local cmp_select { :behavior cmp.SelectBehavior.Select})
(local kind_icons
  { :nvim_lsp "λ"
   :luasnip "⋗"
   :buffer "Ω"
   :path "🖫"
   :latex_symbols ""

   :Text ""
   :Method ""
   :Function ""
   :Constructor ""
   :Field ""
   :Variable ""
   :Class "ﴯ"
   :Interface ""
   :Module ""
   :Property "ﰠ"
   :Unit ""
   :Value ""
   :Enum ""
   :Keyword ""
   :Snippet ""
   :Color ""
   :File ""
   :Reference ""
   :Folder ""
   :EnumMember ""
   :Constant ""
   :Struct ""
   :Event ""
   :Operator ""
   :TypeParameter ""})

(set vim.opt.completeopt [:menu :menuone :noselect])

(cmp.setup
  {:snippet
   {:expand (fn [args] (luasnip.lsp_expand args.body))}
   :window
   {:documentation (cmp.config.window.bordered)
    :completion
    {:winhighlight "Normal:Pmenu,FloatBorder:Pmenu,Search:None"
     :col_offset -1
     :side_padding 0}}

   :formatting
   {:fields [ "menu" "abbr" "kind"]
    :format (fn [entry item]
              (set item.menu (. kind_icons entry.source.name))
              item)}

   :mapping
   {:<C-u> (cmp.mapping.scroll_docs -4)
    :<C-d> (cmp.mapping.scroll_docs 4)
    ; :<C-e> (cmp.mapping.abort)
    :<C-space> (cmp.mapping.complete)
    :<C-p> (cmp.mapping.select_prev_item cmp_select)
    :<C-n> (cmp.mapping.select_next_item cmp_select)
    :<Tab> (cmp.mapping.confirm { :select false}) ;; Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    ; :<CR> (cmp.mapping.confirm { :select false }) ;; Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    ; :<C-e> (lambda [] (when (luasnip.choice_active) (luasnip.change_choice 1))
    :<C-c> (lambda [] (when (luasnip.choice_active) (luasnip.change_choice 1)))
    :<C-e> vim.NIL
    :<CR> vim.NIL}

   :sources
   [{:name :nvim_lsp}
    {:name :luasnip}
    {:name :buffer}
    {:name :dictionary :keyword_length 3}
    {:name :path}
    {:name :conjure}
    {:name :rg :keyword_length 5
     :option {:additional_arguments "--hidden --smart-case"}}
    {:name :lua-latex-symbols}
    {:name :nerdfont}
    {:name :tags}
    {:name :treesitter}
    {:name :cmp_pandoc}
    {:name :lbdb}]})


; (vim.keymap.set [:i :s] "<C-c>" (lambda [] (when (luasnip.choice_active) (luasnip.change_choice 1))) {:desc "Next choice"})

(cmp.setup.filetype [ :dap-repl :dapui_watches :dapui_hover] { :sources [{ :name :dap}]})


; (cmp.event:on
;   :confirm_done
;   (cmp_autopairs.on_confirm_done))

;gray
(vim.cmd "highlight! CmpItemAbbrDeprecated guibg=NONE gui=strikethrough guifg=#A89984")
; blue
(vim.cmd "highlight! CmpItemAbbrMatch guibg=NONE guifg=#FD8218")
; (vim.cmd "highlight! CmpItemAbbrMatch guibg=NONE guifg=#FABD2F")
; (vim.cmd "highlight! CmpItemAbbrMatch guibg=NONE guifg=#569CD6")
(vim.cmd "highlight! link CmpItemAbbrMatchFuzzy CmpItemAbbrMatch")
; light blue
(vim.cmd "highlight! CmpItemKindVariable guibg=NONE guifg=#8EC07C") 
; (vim.cmd "highlight! CmpItemKindVariable guibg=NONE guifg=#9CDCFE") 
(vim.cmd "highlight! link CmpItemKindInterface CmpItemKindVariable")
(vim.cmd "highlight! link CmpItemKindText CmpItemKindVariable")
; pink
(vim.cmd "highlight! CmpItemKindFunction guibg=NONE guifg=#D2859A")
(vim.cmd "highlight! link CmpItemKindMethod CmpItemKindFunction")
; front
(vim.cmd "highlight! CmpItemKindKeyword guibg=NONE guifg=#D4D4D4")
(vim.cmd "highlight! link CmpItemKindProperty CmpItemKindKeyword")
(vim.cmd "highlight! link CmpItemKindUnit CmpItemKindKeyword")
