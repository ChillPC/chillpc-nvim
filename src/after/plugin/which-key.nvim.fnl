(let [which-key (require "which-key")]
  (which-key.setup
    {
     :preset "helix" ; "classic" "helix" "modern"
     :plugins
     {:spelling
      {:enabled false ;; enabling this will show WhichKey when pressing z= to select spelling suggestions
       :suggestions 20}}}) ;; how many suggestions should be shown in the list?

  (which-key.add
    [{1 :<leader><space> :group :alt}
     {1 "<leader>'" :group :harpoon}
     {1 "<leader>\\" :group :term}
     {1 :<leader><Cr> :group :conjure} 
     {1 :<leader><Cr>T :group :filetype} 
     {1 :<leader><Cr>c :group :comment} 
     {1 :<leader><Cr>e :group :evaluate} 
     {1 :<leader><Cr>l :group :logs} 
     {1 :<leader><space>F :group :fzf-env}
     {1 :<leader><space>c :group :cding}
     {1 :<leader><space>s :group :spell}
     {1 :<leader><space>sF :group :french}
     {1 :<leader><space>u :group :packages}
     {1 :<leader>= :group :format}
     {1 :<leader>F :group :pathes}
     {1 :<leader>c :group :lcding}
     {1 :<leader>d :group :dap}
     {1 :<leader>f :group :fuzzy}
     {1 :<leader>g :group :git}
     {1 :<leader>go :group :log}
     {1 :<leader>gs :group :diff}
     {1 :<leader>h :group :hunk}
     {1 :<leader>h! :group :blame}
     {1 :<leader>l :group :lsp}
     {1 :<leader>p :group :preview}
     {1 :<leader>q :group :session}
     {1 :<leader>r :group :convert}
     {1 :<leader>s :group :diff}
     {1 :<leader>t :group :term}
     {1 :<leader>u :group :unicode}
     {1 :<leader>x :group :hex}
     {1 :<leader>z :group :style}
     {1 :<leader>z<Tab> :group :tab}
     {1 :<leader>zp :group :parinfer} 
     {1 := :group :format}]))

; (set vim.o.timeoutlen 500)
