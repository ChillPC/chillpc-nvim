(vim.keymap.set "n" "<leader>f<leader>" vim.cmd.RangerEdit { :desc "Ranger"})
(vim.keymap.set "n" "<leader><leader>f" vim.cmd.RangerEdit { :desc "Ranger" })
(vim.keymap.set "n" "<leader><leader>cf" vim.cmd.RangerCD { :desc "Ranger lcd"})
(vim.keymap.set "n" "<leader>cf" vim.cmd.RangerLCD { :desc "Ranger lcd"})
