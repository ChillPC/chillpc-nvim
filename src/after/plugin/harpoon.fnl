(local kset vim.keymap.set)

(pcall
  (lambda []
    (let [harpoon (require :harpoon)]
      (harpoon.setup)

      (kset :n :<leader>m
            (lambda [] (: (harpoon:list) :add)) 
            {:desc "Mark buffer"})

      (kset :n "<leader>''"
            (lambda [] (harpoon.ui:toggle_quick_menu (harpoon:list))) 
            {:desc "Mark menu"})

      (kset :n "<leader>'h" (lambda [] (: (harpoon:list) :prev)) {:desc "Mark <-"})
      (kset :n "<leader>'l" (lambda [] (: (harpoon:list) :next)) {:desc "Mark ->"})
      (for [i 0 9]
        (kset :n (.. "<leader>'" (tostring i))
              (lambda [] (: (harpoon:list) :select i))
              {:desc (.. "Harpoon to " i)})))))
