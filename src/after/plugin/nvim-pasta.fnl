(pcall (lambda []
         (let [map (require "pasta.mappings")]
                    (vim.keymap.set [ "n" "x" ] "p" map.p)
                    (vim.keymap.set [ "n" "x" ] "P" map.P)
                    (vim.keymap.set [ "n" ] "<C-n>" map.toggle_pin)

                    ((. (require "pasta") :setup)
                     {
                     :next_key (vim.api.nvim_replace_termcodes "<C-n>" true true true)
                     :prev_key (vim.api.nvim_replace_termcodes "<C-p>" true true true)}))))
