(local td (require "todo-comments"))

(td.setup)
(vim.keymap.set "n" "]o" (fn [] (td.jump_next)) { :desc "Next todo comment" })
(vim.keymap.set "n" "[o" (fn [] (td.jump_prev)) { :desc "Previous todo comment" })
