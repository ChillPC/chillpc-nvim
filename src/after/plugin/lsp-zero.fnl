(local lsp_custom (require :lsp-on-attach))
(local lsp (require "lsp-zero"))
(local kset vim.keymap.set)

(lsp.preset "recommended")

(lsp.set_preferences { :suggest_lsp_servers false
                      :set_lsp_keymaps false})
(lsp.setup)

(local cmp (require "cmp"))

(local cmp_select { :behavior cmp.SelectBehavior.Select})

(lsp.on_attach lsp_custom.on_attach)
; (lsp.on_attach) ;sp_custom.on_attach)

(kset "n" "<leader>lz" (lambda [] (vim.cmd.LspInfo)) { :desc "Lsp info"})
(kset "n" "<leader>lo" (lambda [] (vim.cmd.LspStop)) { :desc "Lsp stop"})
(kset "n" "<leader>lx" (lambda [] (vim.cmd.LspStop)) { :desc "Lsp stop"})
(kset "n" "<leader>la" (lambda [] (vim.cmd.LspStart)) { :desc "Lsp start"})


; (vim.diagnostic.set { :signs { :priority 10 } })

; ; TODO : Better place
; (local lint (require "lint")

; (vim.api.nvim_create_autocmd
;   [ "BufWritePost" ]
;   { :callback (lambda [] (lint.try_lint))})


; (set lint.linters_by_ft
;      { :python ["mypy"]}

; ((. (require "lint" :linters_by_ft = {
;   markdown = {'vale',}
; }









    ; (let [opts {:buffer bufnr :remap false :silent true}]
    ;   (kset "n" "[l" vim.diagnostic.goto_prev { :desc " lsp"})
    ;   (kset "n" "]l" vim.diagnostic.goto_next { :desc " lsp"})
    ;   (kset "n" "gl" vim.diagnostic.open_float { :desc "Diagnostic lsp"})

    ;   (kset "n" "gd" vim.lsp.buf.definition { :desc "Definition lsp"})
    ;   (kset "n" "gD" vim.lsp.buf.declaration { :desc "Declaration lsp"})
    ;   (kset "n" "gK" vim.lsp.buf.hover { :desc "Hover info lsp"})
    ;   (kset "n" "gi" vim.lsp.buf.implementation { :desc "Implementation lsp"})
    ;   (kset "n" "gs" vim.lsp.buf.signature_help { :desc "Signature lsp"})
    ;   (kset "n" "<leader>lwa" vim.lsp.buf.add_workspace_folder { :desc "Add workspace"})
    ;   (kset "n" "<leader>lwr" vim.lsp.buf.remove_workspace_folder
    ;                   { :desc "Delete workspace"})
    ;   (kset
    ;     "n" "<leader>lwl"
    ;     (lambda []
    ;       (print (vim.inspect (vim.lsp.buf.list_workspace_folders))))
    ;     { :desc "List workspace"})
    ;   (kset "n" "<leader>ld" vim.lsp.buf.type_definition { :desc "Type definition"})
    ;   (kset "n" "gt" vim.lsp.buf.type_definition { :desc "Type definition"})
    ;   (kset "n" "<leader>la" vim.lsp.buf.code_action { :desc "Actions"})
    ;   (kset "n" "ga" vim.lsp.buf.code_action { :desc "Actions"})
    ;   (kset "n" "gr" vim.lsp.buf.rename { :desc "Rename lsp"})
    ;   (kset "n" "g/" vim.lsp.buf.references { :desc "References lsp"})

    ;   (let [lsp-format (lambda [] (vim.lsp.buf.format { :async true}))]
    ;     (kset "n" "=l" lsp-format { :desc "Format lsp"})
    ;     (kset "n" "g=" lsp-format { :desc "Format lsp"})
    ;     (kset "n" "<leader>l=" lsp-format { :desc "Format lsp"}))))
