(vim.keymap.set "n" "yc" "m`yyP:CommentToggle<Cr>``" {:desc "Copy and comment"})
(vim.keymap.set "v" "yc" "yPV']:CommentToggle<Cr>``" {:desc "Copy and comment"})
