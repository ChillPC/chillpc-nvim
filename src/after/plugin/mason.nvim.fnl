(local lsp_custom (require :lsp-on-attach))

(vim.keymap.set "n" "<leader><leader>ul" (fn [] (vim.cmd.Mason)) { :desc "Mason"})

((. (require :mason) :setup) {})

((. (require :mason-lspconfig) :setup) {})

(local lspconfig (require :lspconfig))

(lspconfig.pylsp.setup
  {:on_attach lsp_custom.on_attach
   :settings
   {:pylsp
    {:plugins
     {:pycodestyle {:maxLineLength 100}}
     :flake8 {:enabled false}
     :rope_autoimport {:enabled true}
     :rope_autocompletion {:enabled true}

     :pylsp-mypy {:enables true :live_mode true :strict false}}}})
     ; :ruff {:enabled true :extendSelect ["I"]}
     ; :black {:enabled  true :line_length 100}}}})

; (local pylsp ((. (require :mason-registry) :get_package) :python-lsp-server))

; (pylsp:on
;   "install:success"
;   (lambda [] (vim.cmd.PylspInstall
;                {:args [:python-lsp-black :pylsp-rope :pylsp-mypy :pyls-flake8 :python-lsp-ruff]})))
; :pyls-isort :pyls-memestra]})))
