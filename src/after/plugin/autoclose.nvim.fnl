(local parinfer-managed ["clojure" "scheme" "lisp" "racket" "hy" "fennel" "janet" "carp" "wast" "yuck"])

(pcall
  (lambda []
    ((. (require :autoclose) :setup)
     {:keys
      {"\"" {:close true :escape true :pair "\"\""}
       "'" {:close true :escape true :pair "''" :disabled_filetypes [:scheme]}
       "(" {:close true :escape false :pair "()" :disabled_filetypes parinfer-managed}
       ")" {:close false :escape true :pair "()" :disabled_filetypes parinfer-managed}
       :> {:close false :escape true :pair "<>"}
       "[" {:close true :escape false :pair "[]" :disabled_filetypes parinfer-managed}
       "]" {:close false :escape true :pair "[]" :disabled_filetypes parinfer-managed}
       "`" {:close true :escape true :pair "``"}
       "{" {:close true :escape false :pair "{}" :disabled_filetypes parinfer-managed}
       "}" {:close false :escape true :pair "{}" :disabled_filetypes parinfer-managed}}
      :options
      {:auto_indent true
       :disable_command_mode false
       :disable_when_touch false
       :disabled_filetypes []
       :pair_spaces false
       :touch_regex "[%w(%[{]"}})))

