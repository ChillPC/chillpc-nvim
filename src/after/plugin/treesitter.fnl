; (local rainbow (require "ts-rainbow"))

(pcall
  (lambda []
    (let [tree-config (require :nvim-treesitter.configs)]
      ((. tree-config :setup)
       {:event ["BufReadPre" "BufNewFile"]
        :sync_install false
        :auto_install false
        :indent {:enable true :disable ["yaml"]}
        :highlight
        {:enable true
         :additional_vim_regex_highlighting true}
        :incremental_selection
        {:enable true
         :keymaps
         {:init_selection :<leader>w
          :node_decremental :<leader>W
          :node_incremental :<leader>w
          :scope_incremental false}}}))))

; ((. (require :nvim-treesitter.configs) :setup) {})

; :rainbow
; { :enable true
;  ; :disable []
;  ; :query "rainbow-parens"
;  :query { :html "rainbow-tags"
;          :latex "rainbow-blocks"}

;  :strategy { :lisp (. rainbow.strategy :local)}
;  ; :strategy (require "ts-rainbow.strategy.global")
;  :hlgroups
;  [ "TSRainbowRed"
;   "TSRainbowYellow"
;   "TSRainbowBlue"
;   "TSRainbowOrange"
;   "TSRainbowGreen"
;   "TSRainbowViolet"
;   "TSRainbowCyan"]
;  ; Do not enable for files with more than n lines
;  :max_file_lines 3000}})
