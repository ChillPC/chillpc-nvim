(local g vim.g)
(local vcmd vim.cmd)
(local vapi vim.api)
(local vfn vim.fn)
(local vnil vim.NIL)


(tset g "conjure#mapping#prefix" :<leader><Cr>)

(tset g "conjure#mapping#enable_ft_mappings" true)

(tset g "conjure#mapping#log_split" :n)
(tset g "conjure#mapping#log_vsplit" :b)
(tset g "conjure#mapping#log_tab" :t)
(tset g "conjure#mapping#log_toggle" :<Tab>)
(tset g "conjure#mapping#log_reset_soft" :lx)
(tset g "conjure#mapping#log_reset_hard" :lX)

(tset g "conjure#mapping#eval_current_form" :<leader>)
(tset g "conjure#mapping#eval_comment_current_form" :c<leader>)
(tset g "conjure#mapping#eval_root_form" :<Cr>)
(tset g "conjure#mapping#eval_comment_root_form" :c<Cr>)
(tset g "conjure#mapping#eval_word" :w)
(tset g "conjure#mapping#eval_comment_word" :cw)

(tset g "conjure#mapping#eval_file" :f)
(tset g "conjure#mapping#eval_buf" :*)
(tset g "conjure#mapping#eval_visual" "")
(tset g "conjure#mapping#eval_motion" :E)
(tset g "conjure#mapping#def_word" :d)
(tset g "conjure#mapping#doc_word" :k)

(tset g "conjure#highlight#enabled" true)

(tset g "conjure#client#fennel#aniseed#aniseed_module_prefix" "aniseed.")

(each [_ v (ipairs ["racket" "scheme" "python"])]
  (tset g (.. :conjure#client# v :#stdio#mapping#start) :A)
  (tset g (.. :conjure#client# v :#stdio#mapping#stop) :x))


;; # Scheme

(tset g "conjure#filetype_suffixes#scheme" [:scm :ss :sld])

(fn enclose-set [set-lambda]
  (lambda []
    (vcmd.ConjureSchemeStop)
    (set-lambda)
    (vcmd.ConjureSchemeStart)))

(vapi.nvim_create_user_command
  :ConjureSchemeGuile
  (enclose-set
    (lambda [_]
      (tset g "conjure#filetype#scheme" "conjure.client.guile.socket")
      (tset g "conjure#client#guile#socket#pipename" "/tmp/guile-socket")))
  {})


(vapi.nvim_create_user_command
  :ConjureSchemeGosh
  (enclose-set
    (lambda [_]
      (tset g :conjure#filetype#scheme "conjure.client.scheme.stdio")
      (tset g :conjure#client#scheme#stdio#command "gosh -i")
      (tset g :conjure#client#scheme#stdio#prompt_pattern "gosh[>\\$] $?")))
  {})


(fn set-chibi []
  (tset g :conjure#filetype#scheme "conjure.client.scheme.stdio")
  (tset g :conjure#client#scheme#stdio#command
        (.. "chibi-scheme"
            (let [guix-env (vfn.getenv "GUIX_ENVIRONMENT")]
              (if (= guix-env vnil)
                ""
                (.. " -I " guix-env "/lib/scheme-libs")))))
  (tset g :conjure#client#scheme#stdio#prompt_pattern "> $?")
  (tset g :conjure#client#scheme#stdio#value_prefix_pattern false))


(vapi.nvim_create_user_command
  :ConjureSchemeChibi
  (enclose-set set-chibi)
  {})


(vapi.nvim_create_user_command
  :ConjureSchemeGambitR7RS
  (enclose-set
    (lambda [_]
      (tset g :conjure#filetype#scheme "conjure.client.scheme.stdio")
      (tset g :conjure#client#scheme#stdio#command
            (.. "gsi -:r7rs"
              (let [guix-env (vfn.getenv "GUIX_ENVIRONMENT")]
                (if (= guix-env vnil)
                  ""
                  (.. ",~~guixlib=" guix-env "/lib/scheme-libs,search=~~guixlib")))
              " -"))
      (tset g :conjure#client#scheme#stdio#prompt_pattern "> $?")
      (tset g :conjure#client#scheme#stdio#value_prefix_pattern false)))
  {})


(vapi.nvim_create_user_command
  :ConjureSchemePetite
  (enclose-set
    (lambda [_] 
      (tset g :conjure#filetype#scheme "conjure.client.scheme.stdio")
      (tset g :conjure#client#scheme#stdio#command "petite")
      (tset g :conjure#client#scheme#stdio#prompt_pattern "> $?")))
  {})


(set-chibi)
