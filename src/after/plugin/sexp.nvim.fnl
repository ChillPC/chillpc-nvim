(local kset vim.keymap.set)
(local cmd vim.cmd)

(pcall
  (lambda []
    ((. (require :sexp) :setup)
     {:enable_insert_mode_mappings false
      :filetypes "clojure,scheme,lisp,timl,fennel"
      :insert_after_wrap true
      :mappings 
      {"sexp_capture_prev_element" "<("
       "sexp_emit_head_element" ">("
       "sexp_emit_tail_element" "<)"
       "sexp_capture_next_element" ">)"}}))) 

    ; (kset :n "<(" "<Plug>(sexp_capture_prev_element)")
    ; (kset :n ">(" "<Plug>(sexp_emit_head_element)")
    ; (kset :n "<)" "<Plug>(sexp_emit_tail_element)")
    ; (kset :n ">)" "<Plug>(sexp_capture_next_element)")))
