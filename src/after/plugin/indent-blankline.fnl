(local cmd vim.cmd)
(local kset vim.keymap.set)

(local highlight [:RainbowRed
                  :RainbowYellow
                  :RainbowBlue
                  :RainbowOrange
                  :RainbowGreen
                  :RainbowViolet
                  :RainbowCyan])

(pcall
  (lambda []
    (let [ibl (require :ibl) hooks (require :ibl.hooks)]
      (hooks.register
        hooks.type.HIGHLIGHT_SETUP
        (lambda [] (vim.api.nvim_set_hl 0 :RainbowRed {:fg "#E06C75"})
          (vim.api.nvim_set_hl 0 :RainbowYellow {:fg "#E5C07B"})
          (vim.api.nvim_set_hl 0 :RainbowBlue {:fg "#61AFEF"})
          (vim.api.nvim_set_hl 0 :RainbowOrange {:fg "#D19A66"})
          (vim.api.nvim_set_hl 0 :RainbowGreen {:fg "#98C379"})
          (vim.api.nvim_set_hl 0 :RainbowViolet {:fg "#C678DD"})
          (vim.api.nvim_set_hl 0 :RainbowCyan {:fg "#56B6C2"})))
      (ibl.setup {:indent {:highlight highlight}})
      (cmd.IBLDisable)
      ; (kset :n :<leader>zi cmd.IBLToggleScope { :desc "Indent blankline buffer"})
      (kset :n :<leader>zi cmd.IBLToggle { :desc "Indent blankline global"}))))
