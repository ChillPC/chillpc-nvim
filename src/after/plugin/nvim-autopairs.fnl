(pcall
  (lambda []
    ((. (require "nvim-autopairs") :setup)
     {
      ; :disable_filetype ["scheme" "racket" "guile" "clojure" "cljs" "lisp"]
      ; :check_ts true  ; Deactivate if not good
      :check_ts false  ; Deactivate if not good
      :enable_check_bracket_line false
      :ignored_next_char ""
      ; :ignored_next_char "[%%%'%[%\"%.%`%$]"
      :fast_wrap
      {:map "<C-s>"
       :end_key "p"
       :keys "qwertyuiopzxcvbnmasdfghjkl"
       :check_comma true
       :highlight "Search"
       :highlight_grey "Comment"
       :manual_position false
       :pattern  "[%'%\"%>%]%)%}%,]"
       ; "[%[%(%{%<%)%>%]%)%}%'%\"%,%.%:%;%=%-% ]"
       ; :pattern "[=[[%'%\"%)%>%]%)%}%,]]=]"
       :chars [ "{" "[" "(" "\"" "'" "*"]}})))
