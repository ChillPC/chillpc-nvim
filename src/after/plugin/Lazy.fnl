(local lazy (require "lazy"))

(vim.keymap.set "n" "<leader><leader>uu" (fn [] (vim.cmd.Lazy)) { :desc "Lazy plug" })
