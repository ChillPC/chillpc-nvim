(local kset vim.keymap.set)
(local vcmd vim.cmd)
(local vfn vim.fn)
(local vg vim.g)
(local vbo vim.bo)
(local vo vim.o)
(local vv vim.v)

; --- Setup

((. (require "toggleterm") :setup)
 {
  ; :open_mapping "<c-\\>"
  :size (lambda [term] (if (= term.direction "horizontal")
                         (* vo.lines 0.3)
                         (* vo.columns 0.4)))

  :highlights
  { :Normal {:bg "none" :guibg "none"}
   :NormalFloat {:link "Normal"}
   :FloatBorder {:guibg "none" :guifg "none"}}
   
  :start_in_insert false

  :winbar
  { :enabled false
   :name_formatter (lambda [term] term.name)}

  :direction "vertical"

  :terminal_mappings true ; whether or not the open mapping applies in the opened terminals
  ; :insert_mappings true ; whether or not the open mapping applies in insert mode
  :autochdir false ; when neovim changes it current directory the terminal will change it's own when next it's opened
  :auto_scroll true
  :persist_size true

  :float_opts { :border "curved"}})

; --- Term window format

(set vg.toggleterm-direction "float")

(fn term-with-direction []
  (vcmd.ToggleTerm [(.. "direction=" vg.toggleterm-direction)]))

(fn change-direction [direction]
  (values (lambda [] (set vg.toggleterm-direction direction)
            (term-with-direction))
          {:desc (.. (string.upper (string.sub direction 1 1))
                     (string.sub direction 2)
                     " Term")}))

(let [(f opt) (change-direction "horizontal")]
  (kset "n" "<leader>th" f opt)
  (kset "n" "<leader>t-" f opt)
  (kset "n" "<leader>t_" f opt)
  (kset "n" "<leader>\\h" f opt)
  (kset "n" "<leader>\\-" f opt)
  (kset "n" "<leader>\\_" f opt))

(let [(f opt) (change-direction "vertical")]
  (kset "n" "<leader>tv" f opt)
  (kset "n" "<leader>t/" f opt)
  (kset "n" "<leader>t|" f opt)
  (kset "n" "<leader>t\\v" f opt)
  (kset "n" "<leader>t\\" f opt))

(let [(f opt) (change-direction "tab")]
  (kset "n" "<leader>tb" f opt)
  (kset "n" "<leader>tt" f opt)
  (kset "n" "<leader>\\b" f opt)
  (kset "n" "<leader>\\t" f opt))

(let [(f opt) (change-direction "float")]
  (kset "n" "<leader>t<leader>" f opt)
  (kset "n" "<leader>t<Cr>" f opt)
  (kset "n" "<leader>tf" f opt)
  (kset "n" "<leader>\\<leader>" f opt)
  (kset "n" "<leader>\\<Cr>" f opt)
  (kset "n" "<leader>\\f" f opt))

; ---

; (set vg.term_to_send "1")

; (local esc (vim.api.nvim_replace_termcodes "<ESC>" true false true))

; (for [i 1 9]
;   (let [str_i (tostring i)]
;     (kset "n" (.. "<leader>t" str_i) (lambda [] (set vg.term_to_send i))
;                     { :desc (.. "Send to term " str_i) })))

; (kset "n" "<C-c>!"
;                 (lambda []
;                   (vim.cmd.TermExec ["cmd='!!\n'"]))
;                 { :desc "Load file" })

; (kset "n" "<C-c><C-c>"
;                 (lambda []
;                   (vim.cmd.TermExec ["cmd='(load\\ \"%\")'"]))
;                 { :desc "Load file" })

; (kset "n" "<C-c><leader>"
;                 (lambda []
;                   (vim.cmd.TermExec ["cmd=',enter\\ \"%\"'"]))
;                 { :desc "Enter file" })

; (kset "n" "<C-c><Cr>"
;                 "vip:ToggleTermSendVisualSelection<Cr>"
;                 { :desc "Send <p> to term" })

; (kset "v" "<C-c>"
;                 ":ToggleTermSendVisualSelection<Cr>"
;                 { :desc "Send to term"})

; --- Toggle term

(let [term
      (lambda []
        (vcmd.ToggleTerm
          {:args [(.. "direction="
                      vg.toggleterm-direction)]
           :count vv.count}))]
          
  (kset ["n" "t"] "<C-\\>" term
        {:desc "Toggle Term"})

  (kset ["n" "t" "i"] "<C-w><Cr>" term
        {:desc "Toggle Term"}))

(for [i 0 9]
  (kset
    "n" (.. "<leader>" (tostring i))
    (lambda []
      (vcmd.ToggleTerm
        {:args
         [(.. "direction="
           vg.toggleterm-direction)]
         :count (if (<= i 0) 10 i)}))
    {:desc (.. "Term " (tostring i))}))


; --- Create /tmp file for yanking content of a term

(fn tmp-cwd-indexed [index]
  (let [(base-file-name _)
        (->
          (vfn.getcwd)
          (string.gsub "/" "_"))]
    (.. "/tmp/" base-file-name "-" (tostring index))))
  
(kset :n :<C-w>y
      (lambda []
        (if (= vbo.filetype "toggleterm")
          (let [tmp-fn (vfn.expand "%:t:s?^.*#toggleterm#??")]
            (vcmd "%yank z")
            (vcmd.tabnew (tmp-cwd-indexed tmp-fn))
            (vcmd "%delete")
            (vcmd "put z"))
          (do
            (vcmd "%yank z")
            (vcmd.tabnew)
            (vcmd "put z"))))
    {:desc "Paste new tab"})

(for [i 0 9]
  (kset
    :n (.. :<C-w>p (tostring i))
    (lambda [] (vcmd.edit (tmp-cwd-indexed i)))))
