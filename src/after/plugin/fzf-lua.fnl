(local kset vim.keymap.set)
(local cmd vim.cmd)
(local vfn vim.fn)


(pcall
  (lambda [])
  (let [fzf (require :fzf-lua)
        fzf-path (require :fzf-lua.path)]
    ; Files
    (kset :n :<leader>ff fzf.files {:desc :Files})

    ; rifle file
    (kset
      :n :<leader>fr<Cr> 
      (lambda []
        (fzf.files
          {:actions
           {:default
            (fn [selected opts]
              (let [path (. (fzf-path.entry_to_file
                              (. selected 1) opts
                              (. opts :force_uri)) :path)]
                (os.execute (.. "rifle \"" path "\""))))}}))
      { :desc :Rifle})

    ; Living editor
    (kset :n :<leader>fb fzf.buffers {:desc :Buffers})
    (kset :n :<leader>f<Tab> fzf.tabs {:desc :Tabs})
    (kset :n :<leader>fp fzf.registers {:desc :Registers})
    (kset :n :<leader>fq fzf.quickfix {:desc :Quickfix})
    (kset :n :<leader>f/ fzf.search_history {:desc :History})

    ; Tag and mar
    (kset :n :<leader>fc fzf.tags {:desc :Tags})
    (kset :n :<leader>fC fzf.btags {:desc "B tags"})
    (kset :n :<leader>fj fzf.jumps {:desc :Jumps})
    (kset :n "<leader>f'" fzf.marks {:desc :Marks})

    ; Grep
    (kset :n :<leader>f<Cr> fzf.live_grep {:desc "Grep live"})
    (kset :v :<leader>f<Cr> fzf.grep_visual {:desc :Grep})

    ; Lines
    (kset :n :<leader>fl fzf.lines {:desc :Lines})
    (kset :n :<leader>fL fzf.blines {:desc "B lines"})

    ; Editor config
    (kset :n :<leader>ft fzf.filetypes {:desc :Filetypes})
    (kset :n :<leader>fT fzf.filetypes {:desc :Filetypes})
    (kset :n :<leader>fO fzf.colorschemes {:desc :Colorscheme})
    (kset :n :<leader>fm fzf.keymaps {:desc :Maps})

    ; Help
    (kset :n :<leader>f? fzf.help_tags {:desc :Help})
    (kset :n :<leader>fM fzf.man_pages {:desc :Man})

    ; Commands
    (kset :n :<leader>fx fzf.builtin {:desc "Lua commands"})
    (kset :n :<leader>f: fzf.commands {:desc :Commands})
    (kset :n "<leader>f;" fzf.command_history {:desc "Command history"})

    ; Git
    (kset :n :<leader>fg fzf.git_files {:desc "Git files"})
    (kset :n :<leader>gf fzf.git_commits {:desc :Commits})
    (kset :n :<leader>gF fzf.git_bcommits {:desc "B commits"})

    ; Dap
    (kset :n :<leader>fdd fzf.dap_breakpoints {:desc "Dap breaks"})
    (kset :n :<leader>fd<Cr> fzf.dap_configurations {:desc "Dap continue"})
    (kset :n :<leader>fdv fzf.dap_variables {:desc "Dap vars"})
    (kset :n :<leader>fdf fzf.dap_frames {:desc "Dap frames"})

    ; Lsp
    (kset :n :<leader>fla fzf.lsp_code_actions {:desc "Lsp Action"})
    (kset :n :ga fzf.lsp_code_actions {:desc "Lsp Action"})
    ; (kset :n "g?" fzf.lsp_finder {:desc "Lsp Finder"})
    (kset :n :<leader>fll fzf.diagnostics_document { :desc "Diagnostic lsp"})

    (kset :n :<leader>flt fzf.lsp_typedefs { :desc "Type definition"})
    (kset :n :<leader>flD fzf.lsp_declarations { :desc "Declaration lsp"})
    (kset :n :<leader>fld fzf.lsp_definitions { :desc "Definition lsp"})
    (kset :n :<leader>fl/ fzf.lsp_references { :desc "References lsp"})

    (kset :n :<leader>f% (lambda [] (fzf.files {:cwd (vfn.expand "%:h")})) {:desc "% "})

    ; ../ Back
    (for [i 1 9]
      (let [path-suffix (faccumulate [rel-path "" i 1 i]
                                     (.. "/.." rel-path))]
        (kset :n (.. :<leader>f i)
              (lambda [] (fzf.files {:cwd (.. (vfn.expand "%:h") path-suffix)}))
              {:desc (.. "% " (tostring i))})

        (kset :n (.. :<leader>F i)
              (lambda [] (fzf.files {:cwd (.. (vfn.getcwd) path-suffix)}))
              {:desc (.. "Project " (tostring i))})))

    ; Env pathes
    (for [i (string.byte :A) (string.byte :z)]
      (let [c (string.char i)
            env-path (os.getenv (.. c :_PATH))]
        (when env-path

          ; Fuzzy edit file
          (kset
            :n (.. :<leader>F c)
            (lambda []
              (fzf.files {:cwd env-path}))
            { :desc (.. "Fzf: " env-path)})

          ; Fuzzy rifle file
          (kset
            :n (.. :<leader>fr c)
            (lambda []
              (fzf.files
                {:cwd env-path
                 :actions
                 {:default
                  (fn [selected opts]
                    (let [path (. (fzf-path.entry_to_file
                                    (. selected 1) opts
                                    (. opts :force_uri)) :path)]
                      (os.execute (.. "rifle \"" path "\""))))}}))
            { :desc (.. :Rifle  env-path)})

          ; Fuzzy Yank file
          (kset
            :n (.. :<leader>fy c)
            (lambda []
              (fzf.files
                {:cwd env-path
                 :actions
                 {:default
                  (fn [selected opts]
                    (let [path (. (fzf-path.entry_to_file
                                    (. selected 1) opts
                                    (. opts :force_uri)) :path)]
                      (cmd (.. "read !cat \'" path "'"))))}}))
            { :desc (.. "Yank " env-path)}))))))
