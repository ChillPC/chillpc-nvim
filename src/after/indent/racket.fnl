(local kset vim.keymap.set)
(local kdel vim.keymap.del)

(set vim.o.commentstring ";; %s")

(kset :n :<leader>K (lambda [] (vim.cmd "<Plug>(RacketDoc)")))
(kdel [:n :x] :K {:buffer true})
(kset :n :K "{")
