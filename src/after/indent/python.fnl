(set vim.bo.shiftwidth 4) ; Number of auto-indent spaces
(set vim.bo.softtabstop 4) ; Number of spaces per Tab
