(require "conceals")
(require "keymap")
(require "plugins")
(require "autocmds")

(local o vim.o)
(local opt vim.opt)
(local cmd vim.cmd)
(local ft vim.filetype)

(ft.add
  {:extension
   {:sls :scheme}})

(cmd.filetype "plugin indent on")

(set o.swapfile false)
(set o.exrc true)

(set o.switchbuf :uselast)

;; +--------------------------------------------------------
;; | Colors and Fonts
;; +--------------------------------------------------------

(cmd.syntax { :args ["on"]})

(set o.termguicolors true)
(set o.background "dark")

;; +--------------------------------------------------------
;; | UI
;; +--------------------------------------------------------

;; --- Editing interface
(set o.scrolloff 20) ; Lines to the cursor when moving vertically

;; --- Text visualisation
(set o.number true) ; Show line numbers
(set o.relativenumber true) ; Show line numbers
(set o.showbreak " ↳") ; Wrap-broken line prefix
(set o.breakindent true)
(set o.wrap false) ; Text is wrapped at graphical end of line
(set o.linebreak true) ; Break lines at word (requires Wrap lines)
(set o.showmatch true) ; Highlight matching brace
(set o.visualbell false) ; Use visual bell (no beeping)

;; (set vim.opt.list true)
;; (vim.opt.listchars:append { :space "␣" })
;; (set vim.opt.listchars {:eol "↲" :tab "▸ " :trail "·"})


;; --- Fold
(set o.foldlevel 10) ; Auto fold at level on opening
(set o.foldcolumn "0") ; Add for seeing folds in the gutter
(set o.foldmethod "indent") ; Fold on indent

(opt.foldopen:remove "block" "search" "hor") ; Do not open block passing over


;; --- Concealing
(set o.conceallevel 2) ; Conceal stuff into other (ex: lambda)
(set o.concealcursor "nvc")

;; ---Cursors
(set o.cursorline true)


;; --- Writing
(set o.autoindent true) ; Auto-indent new lines
(set o.expandtab true) ; Use spaces instead of tabs
(set o.smarttab true) ; Enable smart-tabs
(set o.shiftwidth 2) ; Number of auto-indent spaces
(set o.softtabstop 2) ; Number of spaces per Tab
(set o.smartindent true)


;; --- Search
(set o.magic true) ; Set regex modeto magic
(set o.incsearch true) ; Shows the match while typing
(set o.hlsearch true) ; Highlight found searches
(set o.ignorecase true) ; Search case insensitive...
(set o.smartcase true) ; ... but not when search pattern contains upper case characters

;; --- Command
(set o.showcmd true) ; Show me what I'm typing
(set o.cmdheight 1) ; Height of the command bar

;; --- Pane & Tab
(set o.hidden true) ; Hide other paned withfiled closed
(set o.splitright true) ; Split vertical windows right to the current windows
(set o.splitbelow true) ; Split horizontal windows below to the current windows
(set o.showtabline 2)

;; --- Menu
(set o.wildmenu true) ; Turn on the Wild menu
;; (set vo.path+=**)
(set o.showmode false) ; We show the mode with airline or lightline
(set o.ruler true) ; Showcurrent pos
(set o.laststatus 2) ; Always show the status line


; (vim.cmd.highlight [:Folded "ctermbg=Dark" "guibg=Dark" "ctermfg=Grey" "guifg=LightGrey"])
(cmd.highlight [:Folded "guibg=Dark" "guifg=LightBlue"])
