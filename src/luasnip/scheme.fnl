(local ls (require :luasnip))
(local s ls.snippet)
(local sn ls.snippet_node)
(local isn ls.indent_snippet_node)
(local t ls.text_node)
(local i ls.insert_node)
(local f ls.function_node)
(local c ls.choice_node)
(local d ls.dynamic_node)
(local r ls.restore_node)

[(s {:trig :f :name "False"}
    [(t "#f")])

 (s {:trig :t :name "False"}
    [(t "#t")])

 (s {:trig "(definevar" :name "Define var"}
    [(t "(define ")
     (i 1 "var") (t " ")
     (i 2 "value")
     (t ")")])

 (s {:trig "(define" :name "Define function"}
    [(t "(define (") (i 1 "name") (t [")" "  "])
     (t "(") (i 2 "code") (t "))") ])

 ; (s {:trig "(record" :name "Make record"}
 ;    [(t "(define-record-type ") (i 1 "name") (t ["" ""])
 ;     (isn 1
 ;          [(f (lambda [a]
 ;                (())))

 ;           ])
 ;     ; (t "  (make-") (f (lambda [a] ((string.gmatch (. a 1 1) "%S+"))) [1]) (t [")" ""])
 ;     ; (t "  ") (f (lambda [a] (. a 1 1)) [1]) (t ["?" ""])
 ;     ; (t "  ") (f (lambda [a] (print (. a 1 2))) [1])
 ;     ])

 (s {:trig :lol}
    (c 1 [
        (t "1")
        (t "2")
        (t "3")
        ]))
 ]


; (define-record-type lutin
;   (make-lutin number calorie)
;   lutin?
;   (number lutin-number)
;   (calorie lutin-calorie))


; snippet r "Create record"
; (define-record-type ${1:${VISUAL:record}}
;   (make-`!p snip.rv = t[1]` ${2:fields})
;   `!p snip.rv = t[1]`?
;   `!p snip.rv = "\n  ".join(["({field} {record}-{field})".format(record = t[1], field = field) for field in t[2].split(' ')])`)
; endsnippet

