# ChillPC's Nvim configuration

This configuration is written in fennel and is aimed to be pretty close to an ide while staying pretty simple and comprehensible.

To install it, be sure to have `fennel` and run :

```sh
make install
```
