CC = fennel
FLAGS = --compile
EXEC = init

SRCDIR = ./src
OUTDIR = ./out

SRC = $(shell find $(SRCDIR) -iname "*.fnl")
# SRC = $(wildcard $(SRCDIR)/**.fnl)
OUT = $(SRC:$(SRCDIR)%.fnl=$(OUTDIR)%.lua)

.PHONY: all
all : $(OUT)

.PHONY: install
install: all
ifneq ($(wildcard ~/.config/nvim),)
	@echo 'Already existing config at "~/.config/nvim"'
else
	cp -r ./out ~/.config/nvim
endif

.PHONY: remove
remove:
	rm -rf ~/.config/nvim

.PHONY: clean
clean :
	rm -rf out/*

.PHONY: reinstall
reinstall: clean remove
	make install

./out/%.lua: ./src/%.fnl
	@mkdir -p $(shell dirname $@)
	@$(CC) $(FLAGS) $< > $@
